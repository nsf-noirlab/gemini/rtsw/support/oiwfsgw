%define _prefix /gem_base/epics/support
%define name oiwfsgw
%define repository gemdev
%define debug_package %{nil}
%define arch %(uname -m)
%define checkout %(git log --pretty=format:'%h' -n 1) 

#These global defines are added to prevent stripping
# symbols on vxWorks cross-compiled code
# Getting 'strip' to work is probably only needed for
# building a related debug sub-package
#
# But this prevents all the strip warnings
# mrippa 20120202
%global _enable_debug_package 0
%global debug_package %{nil}
%global __os_install_post /usr/lib/rpm/brp-compress %{nil}

Summary: %{name} Package, a module for EPICS base
Name: %{name}
Version: 1.1.1
Release: 9%{?dist}
License: EPICS Open License
Group: Applications/Engineering
Source0: %{name}-%{version}.tar.gz
ExclusiveArch: %{arch}
Prefix: %{_prefix}
## You may specify dependencies here
BuildRequires: epics-base-devel re2c gemini-ade slalib-devel tcslib-devel geminiRec-devel
Requires: epics-base slalib tcslib geminiRec
## Switch dependency checking off
## AutoReqProv: no

%description
This is the module %{name}.

## If you want to have a devel-package to be generated uncomment the following:
%package devel
Summary: %{name}-devel Package
Group: Development/Gemini
Requires: %{name}
%description devel
This is the module %{name}.

%prep
%setup -q 

%build
make distclean uninstall
make

%install
export DONT_STRIP=1
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r dbd $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r db $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r lib $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r configure $RPM_BUILD_ROOT/%{_prefix}/%{name}
# find $RPM_BUILD_ROOT/%{_prefix}/%{name}/configure -name ".git" -exec rm -rf {} \;


%postun
if [ "$1" = "0" ]; then
	rm -rf %{_prefix}/%{name}
fi


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
   /%{_prefix}/%{name}/lib
   /%{_prefix}/%{name}/dbd
   /%{_prefix}/%{name}/db
   /%{_prefix}/%{name}/configure


%files devel
%defattr(-,root,root)
   /%{_prefix}/%{name}/dbd
   /%{_prefix}/%{name}/db
   /%{_prefix}/%{name}/lib
   /%{_prefix}/%{name}/configure

%changelog
* Mon Jan 11 2021 Roberto Rojas <rrojas@gemini.edu> 1.1.1-9
- 

* Mon Jan 04 2021 Roberto Rojas <rrojas@gemini.edu> 1.1.1-8
- removes astlib.dbd

* Mon Jan 04 2021 Roberto Rojas <rrojas@gemini.edu> 1.1.1-7
- modified spec file

* Mon Jan 04 2021 Roberto Rojas <rrojas@gemini.edu> 1.1.1-6
- spec file modified

* Mon Jan 04 2021 Roberto Rojas <rrojas@gemini.edu> 1.1.1-5
- 

* Mon Jan 04 2021 Roberto Rojas <rrojas@gemini.edu> 1.1.1-4
- 

* Wed Dec 30 2020 Roberto Rojas <rrojas@gemini.edu> 1.1.1-3
- new package built with tito

* Wed Nov 18 2020 rrojas <rrojas@gemini.edu> 1.1.1
- copy based on astlib module
