[schematic2]
uniq 17
[tools]
[detail]
w 1784 403 100 0 n#1 gwSendComm.gwSendComm#77.VALH 2080 392 1536 392 1536 1024 junction
w 912 1115 100 0 n#1 ecad20.ecad20#66.VALH 336 1104 1536 1104 1536 1024 2080 1024 2080 1032 gwSendComm.gwSendComm#78.VALH
w 1816 453 100 0 n#2 gwSendComm.gwSendComm#77.VALG 2080 442 1600 442 1600 1088 junction
w 944 1179 100 0 n#2 gwSendComm.gwSendComm#78.VALG 2080 1082 2080 1088 1600 1088 1600 1168 336 1168 ecad20.ecad20#66.VALG
w 1848 507 100 0 n#3 junction 1664 1136 1664 496 2080 496 2080 492 gwSendComm.gwSendComm#77.VALF
w 976 1243 100 0 n#3 ecad20.ecad20#66.VALF 336 1232 1664 1232 1664 1136 2080 1136 gwSendComm.gwSendComm#78.VALF
w 1880 555 100 0 n#4 junction 1728 1184 1728 544 2080 544 2080 542 gwSendComm.gwSendComm#77.VALE
w 1008 1307 100 0 n#4 ecad20.ecad20#66.VALE 336 1296 1728 1296 1728 1184 2080 1184 2080 1182 gwSendComm.gwSendComm#78.VALE
w 1912 603 100 0 n#5 junction 1792 1232 1792 592 2080 592 gwSendComm.gwSendComm#77.VALD
w 1040 1371 100 0 n#5 ecad20.ecad20#66.VALD 336 1360 1792 1360 1792 1232 2080 1232 gwSendComm.gwSendComm#78.VALD
w 1944 651 100 0 n#6 junction 1856 1280 1856 640 2080 640 2080 642 gwSendComm.gwSendComm#77.VALC
w 1072 1435 100 0 n#6 ecad20.ecad20#66.VALC 336 1424 1856 1424 1856 1280 2080 1280 2080 1282 gwSendComm.gwSendComm#78.VALC
w 1976 699 100 0 n#7 junction 1920 1328 1920 688 2080 688 2080 692 gwSendComm.gwSendComm#77.VALB
w 1104 1499 100 0 n#7 ecad20.ecad20#66.VALB 336 1488 1920 1488 1920 1328 2080 1328 2080 1332 gwSendComm.gwSendComm#78.VALB
w 2008 763 100 0 n#8 junction 1984 1399 1984 752 2080 752 2080 759 gwSendComm.gwSendComm#77.VALA
w 1136 1563 100 0 n#8 ecad20.ecad20#66.VALA 336 1552 1984 1552 1984 1399 2080 1399 gwSendComm.gwSendComm#78.VALA
w 1736 987 100 0 n#9 efanouts.efanouts#91.LNK1 1248 464 1440 464 1440 976 2080 976 2080 972 gwSendComm.gwSendComm#78.SLNK
w 1456 379 100 0 n#10 efanouts.efanouts#91.LNK4 1248 368 1712 368 1712 336 2080 336 gwSendComm.gwSendComm#77.SLNK
w 192 -117 100 0 n#11 efanouts.efanouts#91.SELL 1056 464 768 464 768 -128 -336 -128 inhier.OIWFS.P
w 600 123 100 0 n#12 ecad20.ecad20#66.STLK 336 112 912 112 912 384 1008 384 efanouts.efanouts#91.SLNK
w 1320 1746 100 0 n#13 ecad20.ecad20#66.VAL 336 1744 2352 1744 outhier.VAL.p
w 1590 1652 100 0 n#14 ecad20.ecad20#66.MESS 336 1712 864 1712 864 1648 2352 1648 outhier.MESS.p
w -162 1716 100 0 n#15 inhier.ICID.P -384 1648 -304 1648 -304 1712 16 1712 ecad20.ecad20#66.ICID
w -202 1746 100 0 n#16 inhier.DIR.P -384 1744 16 1744 ecad20.ecad20#66.DIR
s 656 1936 200 0 Trigger a subsystem CAD
s 1808 2000 100 0 $Id: tcsMechanismCad20.sch,v 1.1.1.1 1998/11/08 00:20:43 epics Exp $
s 1808 -336 100 0 20 October 1996
s 2032 -192 100 0 Gemini Telescope Control System
[cell use]
use bc200tr -784 -472 -100 0 frame
xform 0 896 832
use efanouts 1008 247 100 0 efanouts#91
xform 0 1128 400
p 864 366 100 0 0 SELM:Specified
p 1120 240 100 1024 0 name:$(top)$(subcad)$(I)
use gwSendComm 2080 903 100 0 gwSendComm#78
xform 0 2192 1184
p 2140 836 100 0 1 seta:wfsIns $(wfsIns1)
p 2120 856 100 0 1 setb:subc $(subcad)
use gwSendComm 2080 263 100 0 gwSendComm#77
xform 0 2192 544
p 2100 236 100 0 1 seta:wfsIns $(wfsIns2)
p 2120 216 100 0 1 setb:subc $(subcad)
use inhier -328 -168 100 0 OIWFS
xform 0 -336 -128
use inhier -376 1608 100 0 ICID
xform 0 -384 1648
use inhier -376 1704 100 0 DIR
xform 0 -384 1744
use ecad20 40 24 100 0 ecad20#66
xform 0 176 912
p 112 1648 100 0 0 DESC:Trigger a subsystem CAD on START
p 112 752 100 0 0 PREC:4
p 112 206 100 0 -1 SNAM:$(snam)
p 416 1518 100 0 -1 def(OUTA):
p 416 1454 100 0 -1 def(OUTB):
p 416 1390 100 0 -1 def(OUTC):
p 416 1326 100 0 -1 def(OUTD):
p 416 1262 100 0 -1 def(OUTE):
p 416 1198 100 0 -1 def(OUTF):
p 416 1134 100 0 -1 def(OUTG):
p 416 1070 100 0 -1 def(OUTH):
p 416 1006 100 0 -1 def(OUTI):
p 416 942 100 0 -1 def(OUTJ):
p 416 878 100 0 -1 def(OUTK):
p 432 814 100 0 -1 def(OUTL):
p 432 750 100 0 -1 def(OUTM):
p 416 686 100 0 -1 def(OUTN):
p 156 0 100 1024 1 name:$(top)$(subsys)$(subcad)
p 112 848 100 0 1 INAM:gwInit
use outhier 2344 1608 100 0 MESS
xform 0 2336 1648
use outhier 2344 1704 100 0 VAL
xform 0 2336 1744
[comments]
