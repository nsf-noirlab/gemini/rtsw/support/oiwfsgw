[schematic2]
uniq 200
[tools]
[detail]
w 384 1275 100 0 VALC inhier.VALC.P -272 1264 1088 1264 estringouts.valc.DOL
w 408 1723 100 0 VALA inhier.VALA.P -256 1712 1120 1712 estringouts.vala.DOL
w 1114 1579 100 0 n#197 estringouts.vala.FLNK 1376 1696 1472 1696 1472 1568 816 1568 816 1456 1104 1456 estringouts.valb.SLNK
w 1122 1371 100 0 n#195 estringouts.valb.FLNK 1360 1472 1472 1472 1472 1360 832 1360 832 1232 1088 1232 estringouts.valc.SLNK
w 1099 814 100 2 n#193 elongouts.st.DOL 1088 784 1088 784 hwin.hwin#189.in
w 1114 875 100 0 n#191 elongouts.mark.FLNK 1344 1056 1456 1056 1456 864 832 864 832 752 1088 752 elongouts.st.SLNK
w 1122 1163 100 0 n#153 estringouts.valc.FLNK 1344 1248 1472 1248 1472 1152 832 1152 832 1024 1088 1024 elongouts.mark.SLNK
w 1092 1051 100 2 n#142 hwin.hwin#141.in 1088 1056 1088 1056 elongouts.mark.DOL
w 120 1051 100 0 SLNK inhier.SLNK.P -320 1040 608 1040 608 1680 1120 1680 estringouts.vala.SLNK
w 400 1499 100 0 VALB inhier.VALB.P -256 1488 1104 1488 estringouts.valb.DOL
s 1808 -336 100 0 20 October 1996
s 2032 -192 100 0 Gemini Telescope Control System
[cell use]
use inhier -272 1447 100 0 VALB
xform 0 -256 1488
p -304 1424 100 0 0 IO:input
p -368 1392 100 0 0 model:connector
p -368 1360 100 0 0 revision:2.2
use inhier -336 999 100 0 SLNK
xform 0 -320 1040
p -368 976 100 0 0 IO:input
p -432 944 100 0 0 model:connector
p -432 912 100 0 0 revision:2.2
use inhier -272 1671 100 0 VALA
xform 0 -256 1712
use inhier -288 1223 100 0 VALC
xform 0 -272 1264
use estringouts 1104 1383 100 0 valb
xform 0 1232 1456
p 1040 1262 100 0 0 OMSL:closed_loop
p 1440 1408 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subcad).B
p 1216 1376 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1360 1440 75 768 -1 pproc(OUT):PP
use estringouts 1088 1159 100 0 valc
xform 0 1216 1232
p 1024 1038 100 0 0 OMSL:closed_loop
p 1440 1184 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subcad).C
p 1200 1152 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1344 1216 75 768 -1 pproc(OUT):PP
use estringouts 1120 1607 100 0 vala
xform 0 1248 1680
p 1056 1486 100 0 0 OMSL:closed_loop
p 1408 1632 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subcad).A
p 1232 1600 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1376 1664 75 768 -1 pproc(OUT):PP
use elongouts 1088 935 100 0 mark
xform 0 1216 1024
p 928 942 100 0 0 OMSL:closed_loop
p 1392 992 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subcad).DIR
p 1200 928 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1344 992 75 768 -1 pproc(OUT):PP
use elongouts 1088 663 100 0 st
xform 0 1216 752
p 928 670 100 0 0 OMSL:closed_loop
p 1408 720 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subcad).DIR
p 1200 656 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1344 720 75 768 -1 pproc(OUT):PP
use hwin 896 743 100 0 hwin#189
xform 0 992 784
p 899 776 100 0 -1 val(in):$(CAD_START)
use hwin 896 1015 100 0 hwin#141
xform 0 992 1056
p 899 1048 100 0 -1 val(in):$(CAD_MARK)
use bc200tr -784 -472 -100 0 frame
xform 0 896 832
[comments]
