[schematic2]
uniq 72
[tools]
[detail]
w 931 -688 100 0 OIWFS junction 928 -288 928 -1088 2384 -1088 esels.inttimeSel.NVL
w 352 -285 100 0 OIWFS inhier.OIWFS.P -224 -288 928 -288 928 2256 2256 2256 esels.seeingSel.NVL
w 1632 1555 100 0 OIWFS egenSub.outGuideGensub.INPA 2336 1552 928 1552 junction
w 1640 515 100 0 OIWFS junction 928 512 2352 512 egenSub.versionGensub.INPA
w 931 -1400 100 0 OIWFS junction 928 -1088 928 -1712 2384 -1712 esels.headTempSel.NVL
w 931 -2000 100 0 OIWFS junction 928 -1712 928 -2288 2384 -2288 esels.aoRmsSel.NVL
w 2776 -2493 100 0 n#1 esels.aoRmsSel.VAL 2672 -2496 2880 -2496 2880 -2528 3120 -2528 esirs.aoRms.INP
w 2848 -2461 100 0 n#2 esels.aoRmsSel.FLNK 2672 -2464 3024 -2464 3024 -2688 3120 -2688 esirs.aoRms.SLNK
w 2112 -2573 100 0 n#3 eaos.ins2outaoRms.VAL 2016 -2576 2208 -2576 2208 -2448 2384 -2448 esels.aoRmsSel.INPE
w 2072 -2541 100 0 n#4 eaos.ins2outaoRms.FLNK 2016 -2544 2128 -2544 2128 -2704 2384 -2704 esels.aoRmsSel.SLNK
w 1640 -2589 100 0 n#5 eais.ins2aoRms.VAL 1568 -2592 1712 -2592 1712 -2544 1760 -2544 eaos.ins2outaoRms.DOL
w 1656 -2557 100 0 n#6 eais.ins2aoRms.FLNK 1568 -2560 1744 -2560 1744 -2576 1760 -2576 eaos.ins2outaoRms.SLNK
w 2136 -2365 100 0 n#7 eaos.ins1outaoRms.VAL 2000 -2368 2272 -2368 2272 -2352 2384 -2352 esels.aoRmsSel.INPB
w 2064 -2333 100 0 n#8 eaos.ins1outaoRms.FLNK 2000 -2336 2128 -2336 2128 -2464 1200 -2464 1200 -2576 1312 -2576 eais.ins2aoRms.SLNK
w 1624 -2381 100 0 n#9 eais.ins1aoRms.VAL 1584 -2384 1664 -2384 1664 -2336 1744 -2336 eaos.ins1outaoRms.DOL
w 1616 -2349 100 0 n#10 eais.ins1aoRms.FLNK 1584 -2352 1648 -2352 1648 -2368 1744 -2368 eaos.ins1outaoRms.SLNK
w 1616 -1773 100 0 n#11 eais.ins1headTemp.FLNK 1584 -1776 1648 -1776 1648 -1792 1744 -1792 eaos.ins1outheadTemp.SLNK
w 1624 -1805 100 0 n#12 eais.ins1headTemp.VAL 1584 -1808 1664 -1808 1664 -1760 1744 -1760 eaos.ins1outheadTemp.DOL
w 2064 -1757 100 0 n#13 eaos.ins1outheadTemp.FLNK 2000 -1760 2128 -1760 2128 -1888 1200 -1888 1200 -2000 1312 -2000 eais.ins2headTemp.SLNK
w 2136 -1789 100 0 n#14 eaos.ins1outheadTemp.VAL 2000 -1792 2272 -1792 2272 -1776 2384 -1776 esels.headTempSel.INPB
w 1656 -1981 100 0 n#15 eais.ins2headTemp.FLNK 1568 -1984 1744 -1984 1744 -2000 1760 -2000 eaos.ins2outheadTemp.SLNK
w 1640 -2013 100 0 n#16 eais.ins2headTemp.VAL 1568 -2016 1712 -2016 1712 -1968 1760 -1968 eaos.ins2outheadTemp.DOL
w 2072 -1965 100 0 n#17 eaos.ins2outheadTemp.FLNK 2016 -1968 2128 -1968 2128 -2128 2384 -2128 esels.headTempSel.SLNK
w 2112 -1997 100 0 n#18 eaos.ins2outheadTemp.VAL 2016 -2000 2208 -2000 2208 -1872 2384 -1872 esels.headTempSel.INPE
w 2848 -1885 100 0 n#19 esels.headTempSel.FLNK 2672 -1888 3024 -1888 3024 -2112 3120 -2112 esirs.headTemp.SLNK
w 2776 -1917 100 0 n#20 esels.headTempSel.VAL 2672 -1920 2880 -1920 2880 -1952 3120 -1952 esirs.headTemp.INP
w 1656 -1357 100 0 n#21 eais.ins2inttime.FLNK 1568 -1360 1744 -1360 1744 -1376 1760 -1376 eaos.ins2outinttime.SLNK
w 2072 -1341 100 0 n#22 eaos.ins2outinttime.FLNK 2016 -1344 2128 -1344 2128 -1504 2384 -1504 esels.inttimeSel.SLNK
w 2064 -1133 100 0 n#23 eaos.ins1outinttime.FLNK 2000 -1136 2128 -1136 2128 -1264 1200 -1264 1200 -1376 1312 -1376 eais.ins2inttime.SLNK
w 1616 -1149 100 0 n#24 eais.ins1inttime.FLNK 1584 -1152 1648 -1152 1648 -1168 1744 -1168 eaos.ins1outinttime.SLNK
w 1640 -1389 100 0 n#25 eais.ins2inttime.VAL 1568 -1392 1712 -1392 1712 -1344 1760 -1344 eaos.ins2outinttime.DOL
w 1624 -1181 100 0 n#26 eais.ins1inttime.VAL 1584 -1184 1664 -1184 1664 -1136 1744 -1136 eaos.ins1outinttime.DOL
w 2112 -1373 100 0 n#27 eaos.ins2outinttime.VAL 2016 -1376 2208 -1376 2208 -1248 2384 -1248 esels.inttimeSel.INPE
w 2136 -1165 100 0 n#28 eaos.ins1outinttime.VAL 2000 -1168 2272 -1168 2272 -1152 2384 -1152 esels.inttimeSel.INPB
w 2776 -1293 100 0 n#29 esels.inttimeSel.VAL 2672 -1296 2880 -1296 2880 -1328 3120 -1328 esirs.intTime.INP
w 2848 -1261 100 0 n#30 esels.inttimeSel.FLNK 2672 -1264 3024 -1264 3024 -1488 3120 -1488 esirs.intTime.SLNK
w 2032 435 100 0 n#31 estringouts.ins1outversion.OUT 1968 432 2096 432 2096 480 2352 480 egenSub.versionGensub.B
w 1584 243 100 0 n#32 estringinval.ins2version.FLNK 1456 240 1712 240 estringouts.ins2outversion.SLNK
w 2008 259 100 0 n#33 estringouts.ins2outversion.FLNK 1968 256 2048 256 2048 -160 2352 -160 egenSub.versionGensub.SLNK
w 2008 467 100 0 n#34 estringouts.ins1outversion.FLNK 1968 464 2048 464 2048 336 1120 336 1120 224 1200 224 estringinval.ins2version.SLNK
w 1584 451 100 0 n#35 estringinval.ins1version.FLNK 1456 448 1712 448 estringouts.ins1outversion.SLNK
w 1520 211 100 0 n#36 estringinval.ins2version.VAL 1456 208 1584 208 1584 272 1712 272 estringouts.ins2outversion.DOL
w 1528 419 100 0 n#37 estringinval.ins1version.VAL 1456 416 1600 416 1600 480 1712 480 estringouts.ins1outversion.DOL
w 2048 227 100 0 n#38 estringouts.ins2outversion.OUT 1968 224 2128 224 2128 416 2352 416 egenSub.versionGensub.C
w 2720 547 100 0 n#39 egenSub.versionGensub.VALA 2640 544 2800 544 2800 272 3040 272 esirs.version.INP
w 2792 -189 100 0 n#40 egenSub.versionGensub.FLNK 2640 -192 2944 -192 2944 112 3040 112 esirs.version.SLNK
w 2016 1475 100 0 n#41 estringouts.ins1outoutGuide.OUT 1952 1472 2080 1472 2080 1520 2336 1520 egenSub.outGuideGensub.B
w 1568 1283 100 0 n#42 estringinval.ins2outGuide.FLNK 1440 1280 1696 1280 estringouts.ins2outoutGuide.SLNK
w 1992 1299 100 0 n#43 estringouts.ins2outoutGuide.FLNK 1952 1296 2032 1296 2032 880 2336 880 egenSub.outGuideGensub.SLNK
w 1992 1507 100 0 n#44 estringouts.ins1outoutGuide.FLNK 1952 1504 2032 1504 2032 1376 1104 1376 1104 1264 1184 1264 estringinval.ins2outGuide.SLNK
w 1568 1491 100 0 n#45 estringinval.ins1outGuide.FLNK 1440 1488 1696 1488 estringouts.ins1outoutGuide.SLNK
w 1504 1251 100 0 n#46 estringinval.ins2outGuide.VAL 1440 1248 1568 1248 1568 1312 1696 1312 estringouts.ins2outoutGuide.DOL
w 1512 1459 100 0 n#47 estringinval.ins1outGuide.VAL 1440 1456 1584 1456 1584 1520 1696 1520 estringouts.ins1outoutGuide.DOL
w 2032 1267 100 0 n#48 estringouts.ins2outoutGuide.OUT 1952 1264 2112 1264 2112 1456 2336 1456 egenSub.outGuideGensub.C
w 2704 1587 100 0 n#49 egenSub.outGuideGensub.VALA 2624 1584 2784 1584 2784 1312 3024 1312 esirs.outGuide.INP
w 2776 851 100 0 n#50 egenSub.outGuideGensub.FLNK 2624 848 2928 848 2928 1152 3024 1152 esirs.outGuide.SLNK
w 1528 1987 100 0 n#51 eais.ins2seeing.FLNK 1440 1984 1616 1984 1616 1968 1632 1968 eaos.ins2outseeing.SLNK
w 1944 2003 100 0 n#52 eaos.ins2outseeing.FLNK 1888 2000 2000 2000 2000 1840 2256 1840 esels.seeingSel.SLNK
w 1936 2211 100 0 n#53 eaos.ins1outseeing.FLNK 1872 2208 2000 2208 2000 2080 1072 2080 1072 1968 1184 1968 eais.ins2seeing.SLNK
w 1488 2195 100 0 n#54 eais.ins1seeing.FLNK 1456 2192 1520 2192 1520 2176 1616 2176 eaos.ins1outseeing.SLNK
w 1512 1955 100 0 n#55 eais.ins2seeing.VAL 1440 1952 1584 1952 1584 2000 1632 2000 eaos.ins2outseeing.DOL
w 1496 2163 100 0 n#56 eais.ins1seeing.VAL 1456 2160 1536 2160 1536 2208 1616 2208 eaos.ins1outseeing.DOL
w 1984 1971 100 0 n#57 eaos.ins2outseeing.VAL 1888 1968 2080 1968 2080 2096 2256 2096 esels.seeingSel.INPE
w 2008 2179 100 0 n#58 eaos.ins1outseeing.VAL 1872 2176 2144 2176 2144 2192 2256 2192 esels.seeingSel.INPB
w 2648 2051 100 0 n#59 esels.seeingSel.VAL 2544 2048 2752 2048 2752 2016 2992 2016 esirs.seeing.INP
w 2720 2083 100 0 n#60 esels.seeingSel.FLNK 2544 2080 2896 2080 2896 1856 2992 1856 esirs.seeing.SLNK
w 2259 2227 100 0 n#61 hwin.hwin#2.in 2256 2224 2256 2224 esels.seeingSel.INPA
w 2792 -3109 100 0 n#62 esels.stateSel.VAL 2688 -3112 2896 -3112 2896 -3144 3136 -3144 esirs.state.INP
w 2860 -3069 100 0 n#63 esels.stateSel.FLNK 2688 -3080 3040 -3080 3040 -3304 3136 -3304 esirs.state.SLNK
w 2128 -3189 100 0 n#64 eaos.ins2OutState.VAL 2032 -3192 2224 -3192 2224 -3064 2400 -3064 esels.stateSel.INPE
w 2088 -3157 100 0 n#65 eaos.ins2OutState.FLNK 2032 -3160 2144 -3160 2144 -3320 2400 -3320 esels.stateSel.SLNK
w 1656 -3205 100 0 n#66 eais.ins2Sate.VAL 1584 -3208 1728 -3208 1728 -3160 1776 -3160 eaos.ins2OutState.DOL
w 1672 -3173 100 0 n#67 eais.ins2Sate.FLNK 1584 -3176 1760 -3176 1760 -3192 1776 -3192 eaos.ins2OutState.SLNK
w 2152 -2981 100 0 n#68 eaos.ins1outState.VAL 2016 -2984 2288 -2984 2288 -2968 2400 -2968 esels.stateSel.INPB
w 2080 -2949 100 0 n#69 eaos.ins1outState.FLNK 2016 -2952 2144 -2952 2144 -3080 1216 -3080 1216 -3192 1328 -3192 eais.ins2Sate.SLNK
w 1640 -2997 100 0 n#70 eais.ins1State.VAL 1600 -3000 1680 -3000 1680 -2952 1760 -2952 eaos.ins1outState.DOL
w 1632 -2965 100 0 n#71 eais.ins1State.FLNK 1600 -2968 1664 -2968 1664 -2984 1760 -2984 eaos.ins1outState.SLNK
s 2464 -704 500 0 wfsGensub.sch
[cell use]
use bd200tr -1024 -920 -100 0 frame
xform 0 1616 784
p 2608 -688 200 0 1 author:S.M.Beard
p 3120 -720 100 0 0 border:D
p 2608 -768 200 0 1 checked:B.Goodrich
p 3120 -784 200 0 -1 date:$Date: 2002/10/22 21:07:17 $
p 2592 2304 200 0 -1 id:$Id: wfsGensub.sch,v 1.6 2002/10/22 21:07:17 cboyer Exp $
p 3120 -432 200 0 -1 project:Gemini OIWFS
p 2592 -528 200 0 -1 revision:$Revision: 1.6 $
p 3120 -560 200 0 -1 title:Wavefront Sensor genSub Records
use esirs 3120 -1577 100 0 intTime
xform 0 3328 -1424
p 3056 -1872 100 0 0 FTVL:DOUBLE
p 3232 -1584 100 1024 0 name:$(top)$(subsys)$(I)
p 3312 -1648 100 0 1 PREC:4
use esirs 2992 1767 100 0 seeing
xform 0 3200 1920
p 3104 1760 100 1024 0 name:$(top)$(subsys)$(I)
p 3114 1764 100 0 1 FTVL:DOUBLE
p 3184 1696 100 0 1 PREC:2
use esirs 3024 1063 100 0 outGuide
xform 0 3232 1216
p 3136 1056 100 1024 0 name:$(top)$(subsys)$(I)
use esirs 3040 23 100 0 version
xform 0 3248 176
p 3152 16 100 1024 0 name:$(top)$(I)
use esirs 3120 -2201 100 0 headTemp
xform 0 3328 -2048
p 3232 -2208 100 1024 0 name:$(top)$(subsys)$(I)
use esirs 3120 -2777 100 0 aoRms
xform 0 3328 -2624
p 3232 -2784 100 1024 0 name:$(top)$(subsys)$(I)
use esels 2384 -1577 100 0 inttimeSel
xform 0 2528 -1312
p 2080 -3264 100 0 0 PREC:4
p 2096 -1170 100 0 0 SCAN:Passive
p 2496 -1584 100 1024 0 name:$(top)$(subsys)$(I)
p 2352 -1120 75 1280 -1 pproc(INPA):NPP
p 2352 -1216 75 1280 -1 pproc(INPD):NPP
use esels 2256 1767 100 0 seeingSel
xform 0 2400 2032
p 1952 80 100 0 0 PREC:4
p 1968 2174 100 0 0 SCAN:Passive
p 2368 1760 100 1024 0 name:$(top)$(subsys)$(I)
p 2224 2224 75 1280 -1 pproc(INPA):NPP
p 2224 2128 75 1280 -1 pproc(INPD):NPP
use esels 2384 -2201 100 0 headTempSel
xform 0 2528 -1936
p 2080 -3888 100 0 0 PREC:4
p 2096 -1794 100 0 0 SCAN:Passive
p 2496 -2208 100 1024 0 name:$(top)$(subsys)$(I)
p 2352 -1744 75 1280 -1 pproc(INPA):NPP
p 2352 -1840 75 1280 -1 pproc(INPD):NPP
use esels 2384 -2777 100 0 aoRmsSel
xform 0 2528 -2512
p 2080 -4464 100 0 0 PREC:4
p 2096 -2370 100 0 0 SCAN:Passive
p 2496 -2784 100 1024 0 name:$(top)$(subsys)$(I)
p 2352 -2320 75 1280 -1 pproc(INPA):NPP
p 2352 -2416 75 1280 -1 pproc(INPD):NPP
use eaos 1760 -1465 100 0 ins2outinttime
xform 0 1888 -1376
p 1904 -1392 100 0 0 OMSL:closed_loop
p 1728 -1344 75 1280 -1 pproc(DOL):NPP
use eaos 1744 -1257 100 0 ins1outinttime
xform 0 1872 -1168
p 1488 -1186 100 0 0 OMSL:closed_loop
p 1712 -1136 75 1280 -1 pproc(DOL):NPP
p 2000 -1200 75 768 -1 pproc(OUT):NPP
use eaos 1632 1879 100 0 ins2outseeing
xform 0 1760 1968
p 1776 1952 100 0 0 OMSL:closed_loop
p 1600 2000 75 1280 -1 pproc(DOL):NPP
use eaos 1616 2087 100 0 ins1outseeing
xform 0 1744 2176
p 1360 2158 100 0 0 OMSL:closed_loop
p 1584 2208 75 1280 -1 pproc(DOL):NPP
p 1872 2144 75 768 -1 pproc(OUT):NPP
use eaos 1760 -2089 100 0 ins2outheadTemp
xform 0 1888 -2000
p 1904 -2016 100 0 0 OMSL:closed_loop
p 1728 -1968 75 1280 -1 pproc(DOL):NPP
use eaos 1744 -1881 100 0 ins1outheadTemp
xform 0 1872 -1792
p 1488 -1810 100 0 0 OMSL:closed_loop
p 1712 -1760 75 1280 -1 pproc(DOL):NPP
p 2000 -1824 75 768 -1 pproc(OUT):NPP
use eaos 1744 -2457 100 0 ins1outaoRms
xform 0 1872 -2368
p 1488 -2386 100 0 0 OMSL:closed_loop
p 1712 -2336 75 1280 -1 pproc(DOL):NPP
p 2000 -2400 75 768 -1 pproc(OUT):NPP
use eaos 1760 -2665 100 0 ins2outaoRms
xform 0 1888 -2576
p 1904 -2592 100 0 0 OMSL:closed_loop
p 1728 -2544 75 1280 -1 pproc(DOL):NPP
use eais 1312 -1449 100 0 ins2inttime
xform 0 1440 -1376
p 896 -1344 100 0 -1 def(INP):$(wfsIns2)$(subsys)intTime
p 1072 -1360 100 1024 0 name:$(top)$(I)
p 1280 -1344 75 1280 -1 pproc(INP):CA
use eais 1328 -1241 100 0 ins1inttime
xform 0 1456 -1168
p 1072 -1170 100 0 0 SCAN:1 second
p 944 -1136 100 0 -1 def(INP):$(wfsIns1)$(subsys)intTime
p 1296 -1136 75 1280 -1 pproc(INP):CA
use eais 1184 1895 100 0 ins2seeing
xform 0 1312 1968
p 768 2000 100 0 -1 def(INP):$(wfsIns2)$(subsys)seeing
p 944 1984 100 1024 0 name:$(top)$(I)
p 1152 2000 75 1280 -1 pproc(INP):CA
use eais 1200 2103 100 0 ins1seeing
xform 0 1328 2176
p 944 2174 100 0 0 SCAN:1 second
p 816 2208 100 0 -1 def(INP):$(wfsIns1)$(subsys)seeing
p 1168 2208 75 1280 -1 pproc(INP):CA
use eais 1312 -2073 100 0 ins2headTemp
xform 0 1440 -2000
p 896 -1968 100 0 -1 def(INP):$(wfsIns2)$(subsys)headTemp
p 1072 -1984 100 1024 0 name:$(top)$(I)
p 1280 -1968 75 1280 -1 pproc(INP):CA
use eais 1328 -1865 100 0 ins1headTemp
xform 0 1456 -1792
p 1072 -1794 100 0 0 SCAN:1 second
p 944 -1760 100 0 -1 def(INP):$(wfsIns1)$(subsys)headTemp
p 1296 -1760 75 1280 -1 pproc(INP):CA
use eais 1328 -2441 100 0 ins1aoRms
xform 0 1456 -2368
p 1072 -2370 100 0 0 SCAN:1 second
p 944 -2336 100 0 -1 def(INP):$(wfsIns1)$(subsys)aoRms
p 1296 -2336 75 1280 -1 pproc(INP):CA
use eais 1312 -2649 100 0 ins2aoRms
xform 0 1440 -2576
p 896 -2544 100 0 -1 def(INP):$(wfsIns2)$(subsys)aoRms
p 1072 -2560 100 1024 0 name:$(top)$(I)
p 1280 -2544 75 1280 -1 pproc(INP):CA
use egenSub 2336 791 100 0 outGuideGensub
xform 0 2480 1216
p 2113 565 100 0 0 FTA:LONG
p 2113 565 100 0 0 FTB:STRING
p 2113 533 100 0 0 FTC:STRING
p 2113 565 100 0 0 FTVA:STRING
p 2416 736 100 0 1 SNAM:gwOutGuideGensub
p 2448 784 100 1024 0 name:$(top)$(subsys)$(I)
p 2048 1198 100 0 1 INAM:gwInit
use egenSub 2352 -249 100 0 versionGensub
xform 0 2496 176
p 2129 -475 100 0 0 FTA:LONG
p 2129 -475 100 0 0 FTB:STRING
p 2129 -507 100 0 0 FTC:STRING
p 2129 -475 100 0 0 FTVA:STRING
p 2432 -304 100 0 1 SNAM:gwOutGuideGensub
p 2464 -256 100 1024 0 name:$(top)$(subsys)$(I)
p 2353 -279 100 0 1 INAM:gwInit
use estringinval 1184 1191 100 0 ins2outGuide
xform 0 1312 1264
p 768 1328 100 0 -1 def(INP):$(wfsIns2)$(subsys)outGuide
p 1152 1296 75 1280 -1 pproc(INP):CA
use estringinval 1184 1399 100 0 ins1outGuide
xform 0 1312 1472
p 1184 1534 100 0 0 SCAN:.1 second
p 784 1520 100 0 -1 def(INP):$(wfsIns1)$(subsys)outGuide
p 1152 1504 75 1280 -1 pproc(INP):CA
use estringinval 1200 151 100 0 ins2version
xform 0 1328 224
p 928 256 100 0 -1 def(INP):$(wfsIns2)version
p 1168 256 75 1280 -1 pproc(INP):CA
use estringinval 1200 359 100 0 ins1version
xform 0 1328 432
p 1200 494 100 0 0 SCAN:10 second
p 928 448 100 0 -1 def(INP):$(wfsIns1)version
p 1168 464 75 1280 -1 pproc(INP):CA
use estringouts 1696 1207 100 0 ins2outoutGuide
xform 0 1824 1280
p 1632 1086 100 0 0 OMSL:closed_loop
p 1296 1472 100 0 0 SCAN:Passive
use estringouts 1696 1415 100 0 ins1outoutGuide
xform 0 1824 1488
p 1632 1294 100 0 0 OMSL:closed_loop
use estringouts 1712 167 100 0 ins2outversion
xform 0 1840 240
p 1648 46 100 0 0 OMSL:closed_loop
p 1312 432 100 0 0 SCAN:Passive
use estringouts 1712 375 100 0 ins1outversion
xform 0 1840 448
p 1648 254 100 0 0 OMSL:closed_loop
use inhier -240 -329 100 0 OIWFS
xform 0 -224 -288
use hwin 2064 2208 100 0 hwin#2
xform 0 2160 2224
p 2067 2216 100 0 -1 val(in):0.0
use esirs 3136 -3393 100 0 state
xform 0 3344 -3240
p 3248 -3400 100 1024 1 name:$(top)$(I)
use esels 2400 -3393 100 0 stateSel
xform 0 2544 -3128
p 2096 -5080 100 0 0 PREC:4
p 2112 -2986 100 0 0 SCAN:Passive
p 2512 -3400 100 1024 1 name:$(top)wfs:$(I)
p 2368 -2936 75 1280 -1 pproc(INPA):NPP
p 2368 -3032 75 1280 -1 pproc(INPD):NPP
use eaos 1760 -3073 100 0 ins1outState
xform 0 1888 -2984
p 1504 -3002 100 0 0 OMSL:closed_loop
p 1728 -2952 75 1280 -1 pproc(DOL):NPP
p 2016 -3016 75 768 -1 pproc(OUT):NPP
use eaos 1776 -3281 100 0 ins2OutState
xform 0 1904 -3192
p 1920 -3208 100 0 0 OMSL:closed_loop
p 1744 -3160 75 1280 -1 pproc(DOL):NPP
use eais 1344 -3057 100 0 ins1State
xform 0 1472 -2984
p 1088 -2986 100 0 0 SCAN:1 second
p 960 -2952 100 0 1 def(INP):$(wfsIns1)wfs:state
p 1312 -2952 75 1280 -1 pproc(INP):CA
use eais 1328 -3265 100 0 ins2Sate
xform 0 1456 -3192
p 912 -3160 100 0 1 def(INP):$(wfsIns2)wfs:state
p 1088 -3176 100 1024 0 name:$(top)$(I)
p 1296 -3160 75 1280 -1 pproc(INP):CA
[comments]
