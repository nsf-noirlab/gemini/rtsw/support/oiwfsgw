[schematic2]
uniq 6
[tools]
[detail]
w 1602 1371 100 0 n#1 efanouts.efanouts#115.LNK1 1200 288 1248 288 1248 1360 2016 1360 gwSendInitComm.gwSendInitComm#141.SLNK
w 1674 491 100 0 n#2 efanouts.efanouts#115.LNK4 1200 192 1408 192 1408 480 2000 480 gwSendInitComm.gwSendInitComm#142.SLNK
w 1936 971 100 0 n#3 junction 1920 1616 1920 960 2000 960 gwSendInitComm.gwSendInitComm#142.VALB
w 960 1627 100 0 n#3 ecad20.ecad20#118.VALB 48 1616 1920 1616 1920 1838 2016 1838 gwSendInitComm.gwSendInitComm#141.VALB
w 88 -165 100 0 n#4 efanouts.efanouts#115.SELL 1008 288 672 288 672 -176 -448 -176 inhier.OIWFS.P
w 264 379 100 0 n#5 ecad20.ecad20#118.FLNK 48 368 528 368 528 208 960 208 efanouts.efanouts#115.SLNK
s 2032 -192 100 0 Gemini Telescope Control System
s 1808 -336 100 0 20 October 1996
s 368 1856 200 0 Tigger a subsystem CAD
[cell use]
use gwSendInitComm 2016 1319 100 0 gwSendInitComm#141
xform 0 2128 1620
p 2016 1312 100 0 1 seta:wfsIns $(wfsIns1)
use gwSendInitComm 2000 439 100 0 gwSendInitComm#142
xform 0 2112 740
p 1984 416 100 0 1 seta:wfsIns $(wfsIns2)
use ecad20 -272 151 100 0 ecad20#118
xform 0 -112 1040
p -176 1776 100 0 0 DESC:Initialize Signal Processinng
p -176 1520 100 0 0 FTVD:STRING
p -176 1488 100 0 0 FTVE:STRING
p -176 1456 100 0 0 FTVF:STRING
p -176 1424 100 0 0 FTVG:STRING
p -176 1296 100 0 0 FTVK:STRING
p -176 1264 100 0 0 FTVL:STRING
p -176 1232 100 0 0 FTVM:STRING
p -176 1200 100 0 0 FTVN:STRING
p -176 1168 100 0 0 FTVO:STRING
p -176 1136 100 0 0 FTVP:STRING
p -176 752 100 0 0 SNAM:gwCADSeqCommand
p -160 144 100 1024 -1 name:$(top)$(subsys)$(subcad)
p -176 976 100 0 1 INAM:gwInit
use efanouts 960 71 100 0 efanouts#115
xform 0 1080 224
p 816 190 100 0 0 SELM:Specified
p 1072 64 100 1024 0 name:$(top)$(subc)$(I)
use inhier -440 -216 100 0 OIWFS
xform 0 -448 -176
[comments]
