[schematic2]
uniq 87
[tools]
[detail]
w 1426 539 100 0 OIWFS junction 1376 1168 1376 528 1536 528 gwDetSigModeSeq.gwDetSigModeSeq#83.OIWFS
w 898 523 100 0 OIWFS junction 864 1168 864 512 992 512 992 520 gwDetSigInit.gwDetSigInit#79.OIWFS
w 970 1179 100 0 OIWFS inhier.OIWFS.P 48 1168 1952 1168 junction
w 866 1579 100 0 OIWFS junction 864 1168 864 1568 928 1568 gwOiObserve.gwOiObserve#77.OIWFS
w 1970 1675 100 0 OIWFS junction 1952 1168 1952 1664 2048 1664 gwOiStop.gwOiStop#78.OIWFS
w 1970 491 100 0 OIWFS junction 1952 1168 1952 480 2048 480 gwDetSigModeSeqDark.gwDetSigModeSeqDark#84.OIWFS
w 1426 1659 100 0 OIWFS junction 1376 1168 1376 1648 1536 1648 1536 1640 gwDetSigInitFgGain.gwDetSigInitFgGain#86.OIWFS
s 2672 80 100 0 OIWFS specific commands
s 2432 32 100 0 Chris Mayer
s 2704 128 200 0 A&G Sequencer
s 2432 2336 100 0 $Id: agSeqOiwfsCommands.sch,v 1.10 2007/05/18 22:29:21 pedro Exp $
s 2432 144 100 0 Copyright
s 2432 112 100 0 Observatory
s 2432 80 100 0 Sciences Ltd.
[cell use]
use gwDetSigInitFgGain 1536 1511 100 0 gwDetSigInitFgGain#86
xform 0 1648 1792
p 1556 1484 100 0 1 seta:subsys dc:
p 1576 1464 100 0 1 setb:subcad detSigInitFgGain
p 1596 1444 100 0 1 setc:subc dsifg
use gwDetSigModeSeqDark 2048 359 100 0 gwDetSigModeSeqDark#84
xform 0 2160 640
p 2068 332 100 0 1 seta:subsys dc:
p 2088 312 100 0 1 setb:subcad detSigModeSeqDark
p 2108 292 100 0 1 setc:subc dsmsd
use gwDetSigModeSeq 1536 391 100 0 gwDetSigModeSeq#83
xform 0 1648 672
p 1556 364 100 0 1 seta:subsys dc:
p 1576 344 100 0 1 setb:subcad detSigModeSeq
p 1596 324 100 0 1 setc:subc dsms
use gwDetSigInit 992 391 100 0 gwDetSigInit#79
xform 0 1104 672
p 1012 364 100 0 1 seta:subsys dc:
p 1032 344 100 0 1 setb:subcad detSigInit
p 1052 324 100 0 1 setc:subc dsi
use gwOiStop 2048 1543 100 0 gwOiStop#78
xform 0 2160 1824
p 2068 1516 100 0 1 seta:cad oiwfsStopObserve
p 2088 1496 100 0 1 setb:subsys dc:
p 2108 1476 100 0 1 setc:subcad stop
p 2128 1456 100 0 1 setd:snam gwCADwfsObserve
use gwOiObserve 928 1511 100 0 gwOiObserve#77
xform 0 1072 1808
p 948 1484 100 0 1 seta:cad oiwfsObserve
p 968 1464 100 0 1 setb:subsys dc:
p 988 1444 100 0 1 setc:subcad observe
p 1008 1424 100 0 1 setd:snam gwCADwfsObserve
use inhier 32 1127 100 0 OIWFS
xform 0 48 1168
[comments]
