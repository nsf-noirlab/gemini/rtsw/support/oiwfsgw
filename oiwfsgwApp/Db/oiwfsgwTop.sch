[schematic2]
uniq 1
[tools]
[detail]
s 944 768 200 0 at the next level of the schematic hierarchy
s 944 832 200 0 The site dependant is definitions are stored
s 2448 80 100 0 Sciences Ltd.
s 2448 112 100 0 Observatory
s 2448 144 100 0 Copyright
s 896 1936 200 0 by the underlying schematics
s 880 2000 200 0 Its main purpose is to define the "top" macro for use
s 864 2064 200 0 This is the top level schematic for the A&G Sequencer.
s 2432 2336 100 0 $Id: agSeqTop.sch,v 1.14 2013/05/29 16:34:17 pedro Exp $
s 2720 128 200 0 A&G Sequencer
s 2448 32 100 0 Chris Mayer
s 2736 80 100 0 Top level schematic
s 944 720 200 0 to avoid code duplication
[cell use]
use bc200tr -160 -136 -100 0 frame
xform 0 1520 1168
use gwSiteDep 728 992 100 0 gwSiteDep#6
xform 0 1400 1256
p 537 1101 100 0 -1 set0:CAD_MARK 0
p 539 1066 100 0 -1 set1:CAD_START 3
p 549 1026 100 0 -1 set2:CAD_STOP 4
p 534 1291 100 0 -1 set3:CAR BUSY 2
p 542 1251 100 0 -1 set4:CAR_ERROR 3
p 544 1321 100 0 -1 set5:CAR_IDLE 0
p 2196 1494 100 0 -1 set6:NONE 0
p 2203 1449 100 0 -1 set7:GMOS 1
p 2208 1411 100 0 -1 set8:NIRS 2
p 2216 1369 100 0 -1 set9:NIRI 3
p 2221 1331 100 0 -1 set10:F2 4
p 2226 1298 100 0 -1 set11:NIFS 5
p 2221 1261 100 0 -1 set12:NICI 6
p 2231 1221 100 0 -1 set13:GSAOI 7
p 2228 1188 100 0 -1 set14:GPI 8
p 1577 1366 100 0 -1 set15:sad ${oiwfs}
p 1135 1366 100 0 -1 set16:top ${oiwfs}
[comments]
