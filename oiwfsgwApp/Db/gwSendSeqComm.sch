[schematic2]
uniq 196
[tools]
[detail]
w 1067 878 100 2 n#194 elongouts.st.SDIS 1056 832 1056 864 junction
w 1074 1003 100 0 n#194 elongouts.mark.FLNK 1296 1152 1408 1152 1408 992 800 992 800 864 1056 864 elongouts.st.SLNK
w 1060 891 100 2 n#193 elongouts.st.DOL 1056 896 1056 896 hwin.hwin#189.in
w 354 1435 100 0 VALP elongouts.valp.DOL 1040 1424 -272 1424 inhier.VALP.P
w 1082 1275 100 0 n#172 elongouts.valp.FLNK 1296 1424 1424 1424 1424 1264 800 1264 800 1120 1040 1120 elongouts.mark.SLNK
w 1044 1147 100 2 n#142 hwin.hwin#141.in 1040 1152 1040 1152 elongouts.mark.DOL
w 528 1403 100 0 SLNK inhier.SLNK.P -304 1136 64 1136 64 1392 1040 1392 elongouts.valp.SLNK
s 1808 -336 100 0 20 October 1996
s 2032 -192 100 0 Gemini Telescope Control System
[cell use]
use elongouts 1040 1031 100 0 mark
xform 0 1168 1120
p 880 1038 100 0 0 OMSL:closed_loop
p 1344 1088 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subcad).DIR
p 1152 1024 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1296 1088 75 768 -1 pproc(OUT):PP
use elongouts 1056 775 100 0 st
xform 0 1184 864
p 896 782 100 0 0 OMSL:closed_loop
p 1376 832 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subcad).DIR
p 1168 768 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1312 832 75 768 -1 pproc(OUT):PP
use elongouts 1040 1303 100 0 valp
xform 0 1168 1392
p 880 1310 100 0 0 OMSL:closed_loop
p 1392 1360 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subcad).P
p 1152 1296 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1296 1360 75 768 -1 pproc(OUT):PP
use hwin 864 855 100 0 hwin#189
xform 0 960 896
p 867 888 100 0 -1 val(in):$(CAD_START)
use hwin 848 1111 100 0 hwin#141
xform 0 944 1152
p 851 1144 100 0 -1 val(in):$(CAD_MARK)
use inhier -320 1095 100 0 SLNK
xform 0 -304 1136
p -352 1072 100 0 0 IO:input
p -416 1040 100 0 0 model:connector
p -416 1008 100 0 0 revision:2.2
use inhier -288 1383 100 0 VALP
xform 0 -272 1424
use bc200tr -784 -472 -100 0 frame
xform 0 896 832
[comments]
