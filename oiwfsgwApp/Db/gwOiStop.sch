[schematic2]
uniq 17
[tools]
[detail]
w 1832 43 100 0 n#1 junction 1568 688 1568 32 2144 32 2144 40 gwSendComm.gwSendComm#72.VALH
w 904 715 100 0 n#1 ecad8.ecad8#23.VALH 288 704 1568 704 1568 688 2144 688 2144 680 gwSendComm.gwSendComm#73.VALH
w 1864 107 100 0 n#2 junction 1632 736 1632 96 2144 96 2144 90 gwSendComm.gwSendComm#72.VALG
w 936 779 100 0 n#2 ecad8.ecad8#23.VALG 288 768 1632 768 1632 736 2144 736 2144 730 gwSendComm.gwSendComm#73.VALG
w 1896 155 100 0 n#3 junction 1696 784 1696 144 2144 144 2144 140 gwSendComm.gwSendComm#72.VALF
w 968 843 100 0 n#3 ecad8.ecad8#23.VALF 288 832 1696 832 1696 784 2144 784 2144 780 gwSendComm.gwSendComm#73.VALF
w 1928 203 100 0 n#4 junction 1760 832 1760 192 2144 192 2144 190 gwSendComm.gwSendComm#72.VALE
w 1000 907 100 0 n#4 ecad8.ecad8#23.VALE 288 896 1760 896 1760 832 2144 832 2144 830 gwSendComm.gwSendComm#73.VALE
w 1960 251 100 0 n#5 junction 1824 880 1824 240 2144 240 gwSendComm.gwSendComm#72.VALD
w 1032 971 100 0 n#5 ecad8.ecad8#23.VALD 288 960 1824 960 1824 880 2144 880 gwSendComm.gwSendComm#73.VALD
w 1992 299 100 0 n#6 junction 1888 928 1888 288 2144 288 2144 290 gwSendComm.gwSendComm#72.VALC
w 1064 1035 100 0 n#6 ecad8.ecad8#23.VALC 288 1024 1888 1024 1888 928 2144 928 2144 930 gwSendComm.gwSendComm#73.VALC
w 2024 347 100 0 n#7 junction 1952 976 1952 336 2144 336 2144 340 gwSendComm.gwSendComm#72.VALB
w 1096 1099 100 0 n#7 ecad8.ecad8#23.VALB 288 1088 1952 1088 1952 976 2144 976 2144 980 gwSendComm.gwSendComm#73.VALB
w 2056 411 100 0 n#8 junction 2016 1047 2016 400 2144 400 2144 407 gwSendComm.gwSendComm#72.VALA
w 1128 1163 100 0 n#8 gwSendComm.gwSendComm#73.VALA 2144 1047 2016 1047 2016 1152 288 1152 ecad8.ecad8#23.VALA
w 1744 635 100 0 n#9 efanouts.efanouts#85.LNK1 1136 400 1392 400 1392 624 2144 624 2144 620 gwSendComm.gwSendComm#73.SLNK
w 1744 -5 100 0 n#10 efanouts.efanouts#85.LNK4 1136 304 1392 304 1392 -16 2144 -16 gwSendComm.gwSendComm#72.SLNK
w 136 107 100 0 n#11 efanouts.efanouts#85.SELL 944 400 768 400 768 96 -448 96 inhier.OIWFS.P
w 440 491 100 0 n#12 ecad8.ecad8#23.STLK 288 480 640 480 640 320 896 320 efanouts.efanouts#85.SLNK
w 1272 1346 100 0 n#13 ecad8.ecad8#23.VAL 288 1344 2304 1344 outhier.VAL.p
w 1542 1252 100 0 n#14 ecad8.ecad8#23.MESS 288 1312 816 1312 816 1248 2304 1248 outhier.MESS.p
w -210 1316 100 0 n#15 inhier.ICID.P -432 1248 -352 1248 -352 1312 -32 1312 ecad8.ecad8#23.ICID
w -250 1346 100 0 n#16 inhier.DIR.P -432 1344 -32 1344 ecad8.ecad8#23.DIR
s 2032 -192 100 0 Gemini Telescope Control System
s 1808 -336 100 0 20 October 1996
s 1808 2000 100 0 $Id: tcsStopMechCad.sch,v 1.2 1999/11/27 12:20:44 cjm Exp $
s 368 1856 200 0 Trigger a subsystem CAD
s 384 1600 100 0 Note that though this schematic sends a
s 384 1568 100 0 stop directive to a subsystem it is
s 384 1536 100 0 activated by a start directive.
[cell use]
use bc200tr -784 -472 -100 0 frame
xform 0 896 832
use efanouts 896 183 100 0 efanouts#85
xform 0 1016 336
p 752 302 100 0 0 SELM:Specified
p 1008 176 100 1024 0 name:$(top)$(subcad)$(I)
use gwSendComm 2144 551 100 0 gwSendComm#73
xform 0 2256 832
p 2164 524 100 0 1 seta:wfsIns $(wfsIns1)
p 2184 504 100 0 1 setb:subc $(subcad)
use gwSendComm 2144 -89 100 0 gwSendComm#72
xform 0 2256 192
p 2176 -112 100 0 1 seta:wfsIns $(wfsIns2)
p 2184 -136 100 0 1 setb:subc $(subcad)
use inhier -424 1304 100 0 DIR
xform 0 -432 1344
use inhier -424 1208 100 0 ICID
xform 0 -432 1248
use inhier -440 56 100 0 OIWFS
xform 0 -448 96
use outhier 2296 1304 100 0 VAL
xform 0 2288 1344
use outhier 2296 1208 100 0 MESS
xform 0 2288 1248
use ecad8 -8 392 100 0 ecad8#23
xform 0 128 896
p -80 1438 100 0 -1 DESC:Triggers a subsystem CAD to stop
p 80 1142 100 0 0 FTVA:STRING
p 80 1086 100 0 0 FTVB:STRING
p 160 686 100 0 0 FTVH:STRING
p 64 736 100 0 0 PREC:4
p 48 574 100 0 -1 SNAM:$(snam)
p 352 1118 100 0 -1 def(OUTA):
p 352 1054 100 0 -1 def(OUTB):
p 352 990 100 0 -1 def(OUTC):
p 352 926 100 0 -1 def(OUTD):
p 352 862 100 0 -1 def(OUTE):
p 352 798 100 0 -1 def(OUTF):
p 352 734 100 0 0 def(OUTG):0.0
p 368 670 100 0 0 def(OUTH):0.0
p 32 334 100 0 1 name:$(top)$(subsys)$(subcad)
p 288 1120 75 768 -1 pproc(OUTA):NPP
p 288 1056 75 768 -1 pproc(OUTB):NPP
p 288 672 75 768 -1 pproc(OUTH):NPP
p 64 832 100 0 1 INAM:gwInit
[comments]
