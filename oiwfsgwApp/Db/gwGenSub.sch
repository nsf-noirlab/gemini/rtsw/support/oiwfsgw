[schematic2]
uniq 139
[tools]
[detail]
w 1098 859 100 0 n#138 egenSub.egenSub#119.OUTD 304 848 1952 848 1952 1680 2304 1680 egenSub.egenSub#120.D
w 1066 923 100 0 n#137 egenSub.egenSub#119.OUTC 304 912 1888 912 1888 1744 2304 1744 egenSub.egenSub#120.C
w 1034 987 100 0 n#136 egenSub.egenSub#119.OUTB 304 976 1824 976 1824 1808 2304 1808 egenSub.egenSub#120.B
w 1002 1051 100 0 n#135 egenSub.egenSub#119.OUTA 304 1040 1760 1040 1760 1872 2304 1872 egenSub.egenSub#120.A
w 1002 603 100 0 n#130 egenSub.egenSub#119.OUTH 304 592 1760 592 1760 480 2304 480 egenSub.egenSub#121.D
w 1034 667 100 0 n#129 egenSub.egenSub#119.OUTG 304 656 1824 656 1824 528 2304 528 2304 544 egenSub.egenSub#121.C
w 1066 731 100 0 n#128 egenSub.egenSub#119.OUTF 304 720 1888 720 1888 608 2304 608 egenSub.egenSub#121.B
w 1098 795 100 0 n#127 egenSub.egenSub#119.OUTE 304 784 1952 784 1952 672 2304 672 egenSub.egenSub#121.A
w 1666 219 100 0 n#126 efanouts.efanouts#122.LNK1 1280 208 2112 208 2112 1168 2304 1168 egenSub.egenSub#120.SLNK
w 1890 -21 100 0 n#125 efanouts.efanouts#122.LNK4 1280 112 1536 112 1536 -32 2304 -32 egenSub.egenSub#121.SLNK
w 554 347 100 0 n#124 egenSub.egenSub#119.FLNK 304 336 864 336 864 128 1040 128 efanouts.efanouts#122.SLNK
w 322 -277 100 0 OIWFS inhier.OIWFS.P -224 -288 928 -288 928 208 1088 208 efanouts.efanouts#122.SELL
s 112 2224 500 0 OIWFS - WFS genSub records
s 2464 -704 500 512 wfsGensub.sch
[cell use]
use inhier -240 -329 100 0 OIWFS
xform 0 -224 -288
use efanouts 1040 -9 100 0 efanouts#122
xform 0 1160 144
p 896 110 100 0 0 SELM:Specified
p 1424 -48 100 1024 1 name:$(top)$(subsys)$(subcad)$(I)
p 1312 208 75 1280 -1 pproc(LNK1):NPP
p 1312 112 75 1280 -1 pproc(LNK4):NPP
use egenSub 2304 -121 100 0 egenSub#121
xform 0 2448 304
p 2416 160 100 0 0 FTA:STRING
p 2081 -347 100 0 0 FTB:STRING
p 2081 -379 100 0 0 FTC:STRING
p 2081 -347 100 0 0 FTVA:STRING
p 2081 -347 100 0 0 FTVB:STRING
p 2081 -379 100 0 0 FTVC:STRING
p 2416 448 100 0 1 NOD:$(nvals)
p 2416 400 100 0 1 NOVD:$(nvals)
p 2368 -192 100 0 1 SNAM:$(snamSend)
p 2656 448 100 0 -1 def(OUTD):$(wfsIns2)$(subsys)$(subcad).J
p 2608 -144 100 1024 1 name:$(top)$(subsys)$(wfsIns2)$(subcad)
p 2256 650 75 0 -1 pproc(INPA):NPP
p 2256 586 75 0 -1 pproc(INPB):NPP
p 2256 522 75 0 -1 pproc(INPC):NPP
p 2256 458 75 0 -1 pproc(INPD):NPP
p 2592 458 75 0 -1 pproc(OUTD):CA
use egenSub 2304 1079 100 0 egenSub#120
xform 0 2448 1504
p 2081 853 100 0 0 FTA:STRING
p 2081 853 100 0 0 FTB:STRING
p 2081 821 100 0 0 FTC:STRING
p 2081 853 100 0 0 FTVA:STRING
p 2081 853 100 0 0 FTVB:STRING
p 2081 821 100 0 0 FTVC:STRING
p 2400 1664 100 0 1 NOD:$(nvals)
p 2400 1632 100 0 1 NOVD:$(nvals)
p 2081 245 100 0 0 NOVI:1
p 2368 1024 100 0 1 SNAM:$(snamSend)
p 2688 1648 100 0 -1 def(OUTD):$(wfsIns1)$(subsys)$(subcad).J
p 2560 1056 100 1024 1 name:$(top)$(subsys)$(wfsIns1)$(subcad)
p 2256 1850 75 0 -1 pproc(INPA):NPP
p 2256 1786 75 0 -1 pproc(INPB):NPP
p 2256 1722 75 0 -1 pproc(INPC):NPP
p 2256 1658 75 0 -1 pproc(INPD):NPP
p 2592 1840 75 0 -1 pproc(OUTA):NPP
p 2592 1658 75 0 -1 pproc(OUTD):CA
use egenSub 16 279 100 0 egenSub#119
xform 0 160 704
p -141 1019 100 0 0 DESC:Receive TCS Data
p -207 53 100 0 0 FTA:STRING
p -207 53 100 0 0 FTB:STRING
p -207 21 100 0 0 FTC:STRING
p -207 53 100 0 0 FTVA:STRING
p -207 53 100 0 0 FTVB:STRING
p -207 21 100 0 0 FTVC:STRING
p -207 -43 100 0 0 FTVE:STRING
p -207 -107 100 0 0 FTVF:STRING
p -207 -107 100 0 0 FTVG:STRING
p -207 -587 100 0 0 NOJ:$(nvals)
p -207 -395 100 0 0 NOVD:$(nvals)
p -207 -523 100 0 0 NOVH:$(nvals)
p 64 192 100 0 1 SCAN:Passive
p 64 224 100 0 1 SNAM:$(snam)
p 240 256 100 1024 1 name:$(top)$(subsys)$(subcad)
p 304 1050 75 0 -1 pproc(OUTA):NPP
p 304 986 75 0 -1 pproc(OUTB):NPP
p 304 922 75 0 -1 pproc(OUTC):NPP
p 304 858 75 0 -1 pproc(OUTD):NPP
use bd200tr -1024 -920 -100 0 frame
xform 0 1616 784
p 2608 -688 200 0 1 author:S.M.Beard
p 3120 -720 100 0 0 border:D
p 2608 -768 200 0 1 checked:B.Goodrich
p 3120 -784 200 0 -1 date:$Date: 2002/10/22 21:07:17 $
p 2592 2304 200 0 -1 id:$Id: wfsGensub.sch,v 1.6 2002/10/22 21:07:17 cboyer Exp $
p 3120 -432 200 0 -1 project:Gemini OIWFS
p 2592 -528 200 0 -1 revision:$Revision: 1.6 $
p 3120 -560 200 0 -1 title:Wavefront Sensor genSub Records
[comments]
