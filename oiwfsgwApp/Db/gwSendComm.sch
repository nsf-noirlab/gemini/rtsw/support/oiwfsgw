[schematic2]
uniq 128
[tools]
[detail]
w 322 43 100 0 SLNK inhier.SLNK.P -256 32 960 32 960 1552 1408 1552 estringouts.vala.SLNK
w 546 539 100 0 VALH inhier.VALH.P -256 528 1408 528 estringouts.valh.DOL
w 546 699 100 0 VALG inhier.VALG.P -256 688 1408 688 estringouts.valg.DOL
w 546 827 100 0 VALF inhier.VALF.P -256 816 1408 816 estringouts.valf.DOL
w 546 955 100 0 VALE inhier.VALE.P -256 944 1408 944 estringouts.vale.DOL
w 546 1115 100 0 VALD inhier.VALD.P -256 1104 1408 1104 estringouts.vald.DOL
w 546 1275 100 0 VALC inhier.VALC.P -256 1264 1408 1264 estringouts.valc.DOL
w 546 1435 100 0 VALB inhier.VALB.P -256 1424 1408 1424 estringouts.valb.DOL
w 1490 395 100 0 n#116 estringouts.valh.FLNK 1664 512 1760 512 1760 384 1280 384 1280 256 1408 256 elongouts.mark.SLNK
w 1474 587 100 0 n#115 estringouts.valg.FLNK 1664 672 1760 672 1760 576 1248 576 1248 496 1408 496 estringouts.valh.SLNK
w 560 1595 100 0 VALA inhier.VALA.P -240 1584 1408 1584 estringouts.vala.DOL
w 1480 731 100 0 n#81 estringouts.valf.FLNK 1664 800 1760 800 1760 720 1248 720 1248 656 1408 656 estringouts.valg.SLNK
w 1480 859 100 0 n#80 estringouts.vale.FLNK 1664 928 1760 928 1760 848 1248 848 1248 784 1408 784 estringouts.valf.SLNK
w 1480 1003 100 0 n#79 estringouts.vald.FLNK 1664 1088 1760 1088 1760 992 1248 992 1248 912 1408 912 estringouts.vale.SLNK
w 1480 1163 100 0 n#78 estringouts.valc.FLNK 1664 1248 1760 1248 1760 1152 1248 1152 1248 1072 1408 1072 estringouts.vald.SLNK
w 1480 1323 100 0 n#77 estringouts.valb.FLNK 1664 1408 1760 1408 1760 1312 1248 1312 1248 1232 1408 1232 estringouts.valc.SLNK
w 1480 1483 100 0 n#76 estringouts.vala.FLNK 1664 1568 1760 1568 1760 1472 1248 1472 1248 1392 1408 1392 estringouts.valb.SLNK
w 1336 34 100 0 n#54 hwin.hwin#52.in 1312 32 1408 32 elongouts.start.DOL
w 1336 290 100 0 n#53 hwin.hwin#51.in 1312 288 1408 288 elongouts.mark.DOL
w 1480 130 100 0 n#49 elongouts.mark.FLNK 1664 288 1728 288 1728 128 1280 128 1280 0 1408 0 elongouts.start.SLNK
s 368 1856 200 0 Send Command to a subsystem CAD
s 1808 -336 100 0 20 October 1996
s 2032 -192 100 0 Gemini Telescope Control System
[cell use]
use elongouts 1432 168 100 0 mark
xform 0 1536 256
p 1456 350 100 0 -1 DESC:Mark directive
p 1248 174 100 0 0 OMSL:closed_loop
p 1744 222 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subc).DIR
p 1664 160 100 1024 -1 name:$(top)$(wfsIns)$(subc)$(I)
p 1664 224 75 768 -1 pproc(OUT):PP
use elongouts 1408 -89 100 0 start
xform 0 1536 0
p 1520 0 100 0 0 OMSL:closed_loop
p 1744 -32 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subc).DIR
p 1712 -96 100 1024 -1 name:$(top)$(wfsIns)$(subc)$(I)
p 1664 -32 75 768 -1 pproc(OUT):PP
use estringouts 1408 1479 100 0 vala
xform 0 1536 1552
p 1344 1358 100 0 0 OMSL:closed_loop
p 1776 1536 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subc).A
p 1520 1472 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1664 1536 75 768 -1 pproc(OUT):PP
p 1712 1520 100 0 0 typ(OUT):path
use estringouts 1408 1319 100 0 valb
xform 0 1536 1392
p 1344 1198 100 0 0 OMSL:closed_loop
p 1792 1376 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subc).B
p 1520 1312 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1664 1376 75 768 -1 pproc(OUT):PP
use estringouts 1408 1159 100 0 valc
xform 0 1536 1232
p 1344 1038 100 0 0 OMSL:closed_loop
p 1792 1216 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subc).C
p 1520 1152 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1664 1216 75 768 -1 pproc(OUT):PP
p 1712 1200 100 0 0 typ(OUT):path
use estringouts 1408 999 100 0 vald
xform 0 1536 1072
p 1344 878 100 0 0 OMSL:closed_loop
p 1792 1056 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subc).D
p 1520 992 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1664 1056 75 768 -1 pproc(OUT):PP
p 1712 1040 100 0 0 typ(OUT):path
use estringouts 1408 839 100 0 vale
xform 0 1536 912
p 1344 718 100 0 0 OMSL:closed_loop
p 1792 928 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subc).E
p 1520 832 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1664 896 75 768 -1 pproc(OUT):PP
p 1712 880 100 0 0 typ(OUT):path
use estringouts 1408 711 100 0 valf
xform 0 1536 784
p 1344 590 100 0 0 OMSL:closed_loop
p 1792 768 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subc).F
p 1520 704 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1664 768 75 768 -1 pproc(OUT):PP
p 1712 752 100 0 0 typ(OUT):path
use estringouts 1408 583 100 0 valg
xform 0 1536 656
p 1344 462 100 0 0 OMSL:closed_loop
p 1792 640 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subc).G
p 1520 576 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1664 640 75 768 -1 pproc(OUT):PP
p 1712 624 100 0 0 typ(OUT):path
use estringouts 1408 423 100 0 valh
xform 0 1536 496
p 1344 302 100 0 0 OMSL:closed_loop
p 1792 480 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subc).H
p 1520 416 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1664 480 75 768 -1 pproc(OUT):PP
p 1712 464 100 0 0 typ(OUT):path
use inhier -256 1543 100 0 VALA
xform 0 -240 1584
p -288 1520 100 0 0 IO:input
p -352 1488 100 0 0 model:connector
p -352 1456 100 0 0 revision:2.2
use inhier -272 1383 100 0 VALB
xform 0 -256 1424
p -304 1360 100 0 0 IO:input
p -368 1328 100 0 0 model:connector
p -368 1296 100 0 0 revision:2.2
use inhier -272 1223 100 0 VALC
xform 0 -256 1264
p -304 1200 100 0 0 IO:input
p -368 1168 100 0 0 model:connector
p -368 1136 100 0 0 revision:2.2
use inhier -272 1063 100 0 VALD
xform 0 -256 1104
p -304 1040 100 0 0 IO:input
p -368 1008 100 0 0 model:connector
p -368 976 100 0 0 revision:2.2
use inhier -272 903 100 0 VALE
xform 0 -256 944
p -304 880 100 0 0 IO:input
p -368 848 100 0 0 model:connector
p -368 816 100 0 0 revision:2.2
use inhier -272 775 100 0 VALF
xform 0 -256 816
p -304 752 100 0 0 IO:input
p -368 720 100 0 0 model:connector
p -368 688 100 0 0 revision:2.2
use inhier -272 647 100 0 VALG
xform 0 -256 688
p -304 624 100 0 0 IO:input
p -368 592 100 0 0 model:connector
p -368 560 100 0 0 revision:2.2
use inhier -272 -9 100 0 SLNK
xform 0 -256 32
p -304 -32 100 0 0 IO:input
p -368 -64 100 0 0 model:connector
p -368 -96 100 0 0 revision:2.2
use inhier -272 487 100 0 VALH
xform 0 -256 528
p -304 464 100 0 0 IO:input
p -368 432 100 0 0 model:connector
p -368 400 100 0 0 revision:2.2
use hwin 1144 -8 100 0 hwin#52
xform 0 1216 32
p 1123 24 100 0 -1 val(in):$(CAD_START)
use hwin 1144 248 100 0 hwin#51
xform 0 1216 288
p 1123 280 100 0 -1 val(in):$(CAD_MARK)
use bc200tr -784 -472 -100 0 frame
xform 0 896 832
[comments]
