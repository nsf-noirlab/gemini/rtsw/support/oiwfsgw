[schematic2]
uniq 90
[tools]
[detail]
w 994 -1909 100 0 n#1 eais.ins2aoZvald.FLNK 1216 -1792 1280 -1792 1280 -1920 768 -1920 768 -2000 960 -2000 eais.ins2aoZvale.SLNK
w 1314 -1813 100 0 n#2 eais.ins2aoZvald.VAL 1216 -1824 1472 -1824 eaos.ins2aoZourvald.DOL
w 1314 -2005 100 0 n#3 eais.ins2aoZvale.VAL 1216 -2016 1472 -2016 eaos.ins2aoZoutvale.DOL
w 1906 -2037 100 0 n#4 eaos.ins2aoZoutvale.VAL 1728 -2048 2144 -2048 2144 -1696 2336 -1696 egenSub.aoZ.INPE
w 1890 -1845 100 0 n#5 eaos.ins2aoZourvald.VAL 1728 -1856 2112 -1856 2112 -1632 2336 -1632 egenSub.aoZ.INPD
w 2594 -1141 100 0 OIWFS junction 928 -288 928 -1152 4320 -1152 4320 -1312 4576 -1312 egenSubE.fgDiag2P2.INPA
w 2642 1707 100 0 OIWFS junction 928 1552 928 1696 4416 1696 4416 1568 4576 1568 egenSubE.fgDiag1P2.INPA
w 1602 1563 100 0 OIWFS inhier.OIWFS.P -224 -288 928 -288 928 1552 2336 1552 egenSub.ttf.INPA
w 1602 -277 100 0 OIWFS junction 928 -288 2336 -288 egenSub.ao.INPA
w 2162 -1429 100 0 OIWFS junction 2048 -1152 2048 -1440 2336 -1440 egenSub.aoZ.INPA
w 994 -1525 100 0 n#6 eais.ins1aoZvald.FLNK 1216 -1408 1280 -1408 1280 -1536 768 -1536 768 -1616 960 -1616 eais.ins1aoZvale.SLNK
w 1314 -1621 100 0 n#7 eais.ins1aoZvale.VAL 1216 -1632 1472 -1632 eaos.ins1aoZoutvale.DOL
w 1314 -1429 100 0 n#8 eais.ins1aoZvald.VAL 1216 -1440 1472 -1440 eaos.ins1aoZoutvald.DOL
w 1858 -1653 100 0 n#9 eaos.ins1aoZoutvale.VAL 1728 -1664 2048 -1664 2048 -1568 2336 -1568 egenSub.aoZ.INPC
w 1858 -1461 100 0 n#10 eaos.ins1aoZoutvald.VAL 1728 -1472 2048 -1472 2048 -1504 2336 -1504 egenSub.aoZ.INPB
w 3234 -2357 100 0 n#11 eais.ins2fgdiag2valb.FLNK 3456 -2240 3520 -2240 3520 -2368 3008 -2368 3008 -2448 3200 -2448 eais.ins2fgdiag2valc.SLNK
w 4354 -1525 100 0 n#12 eaos.insfgdiag2outvalc.VAL 3968 -2496 4192 -2496 4192 -1536 4576 -1536 egenSubE.fgDiag2P2.INPH
w 3234 -2549 100 0 n#13 eais.ins2fgdiag2valc.FLNK 3456 -2432 3520 -2432 3520 -2560 3008 -2560 3008 -2640 3200 -2640 eais.ins2fgdiag2vald.SLNK
w 3554 -2453 100 0 n#14 eais.ins2fgdiag2valc.VAL 3456 -2464 3712 -2464 eaos.insfgdiag2outvalc.DOL
w 4242 -1333 100 0 n#15 eaos.ins1fgdiag2outvala.VAL 3968 -1344 4576 -1344 egenSubE.fgDiag2P2.INPB
w 3234 -1397 100 0 n#16 eais.ins1fgdiag2vala.FLNK 3456 -1280 3520 -1280 3520 -1408 3008 -1408 3008 -1488 3200 -1488 eais.ins1fgdiag2valb.SLNK
w 3554 -1301 100 0 n#17 eais.ins1fgdiag2vala.VAL 3456 -1312 3712 -1312 eaos.ins1fgdiag2outvala.DOL
w 4370 -1557 100 0 n#18 eaos.ins2fgdiag2outvald.VAL 3968 -2688 4224 -2688 4224 -1568 4576 -1568 egenSubE.fgDiag2P2.INPI
w 3554 -2645 100 0 n#19 eais.ins2fgdiag2vald.VAL 3456 -2656 3712 -2656 eaos.ins2fgdiag2outvald.DOL
w 4338 -1493 100 0 n#20 eaos.ins2fgdiag2outvalb.VAL 3968 -2304 4160 -2304 4160 -1504 4576 -1504 egenSubE.fgDiag2P2.INPG
w 4322 -1461 100 0 n#21 eaos.ins2fgdiag2outvala.VAL 3968 -2112 4128 -2112 4128 -1472 4576 -1472 egenSubE.fgDiag2P2.INPF
w 4306 -1429 100 0 n#22 eaos.ins1fgdiag2outvald.VAL 3968 -1920 4096 -1920 4096 -1440 4576 -1440 egenSubE.fgDiag2P2.INPE
w 4290 -1397 100 0 n#23 eaos.ins1fgdiag2outvalc.VAL 3968 -1728 4064 -1728 4064 -1408 4576 -1408 egenSubE.fgDiag2P2.INPD
w 4274 -1365 100 0 n#24 eaos.ins1fgdiag2outvalb.VAL 3968 -1536 4032 -1536 4032 -1376 4576 -1376 egenSubE.fgDiag2P2.INPC
w 3234 -2165 100 0 n#25 eais.ins2fgdiag2vala.FLNK 3456 -2048 3520 -2048 3520 -2176 3008 -2176 3008 -2256 3200 -2256 eais.ins2fgdiag2valb.SLNK
w 3234 -1973 100 0 n#26 eais.ins1fgdiag2vald.FLNK 3456 -1856 3520 -1856 3520 -1984 3008 -1984 3008 -2064 3200 -2064 eais.ins2fgdiag2vala.SLNK
w 3234 -1781 100 0 n#27 eais.ins1fgdiag2valc.FLNK 3456 -1664 3520 -1664 3520 -1792 3008 -1792 3008 -1872 3200 -1872 eais.ins1fgdiag2vald.SLNK
w 3234 -1589 100 0 n#28 eais.ins1fgdiag2valb.FLNK 3456 -1472 3520 -1472 3520 -1600 3008 -1600 3008 -1680 3200 -1680 eais.ins1fgdiag2valc.SLNK
w 3554 -2261 100 0 n#29 eais.ins2fgdiag2valb.VAL 3456 -2272 3712 -2272 eaos.ins2fgdiag2outvalb.DOL
w 3554 -2069 100 0 n#30 eais.ins2fgdiag2vala.VAL 3456 -2080 3712 -2080 eaos.ins2fgdiag2outvala.DOL
w 3554 -1877 100 0 n#31 eais.ins1fgdiag2vald.VAL 3456 -1888 3712 -1888 eaos.ins1fgdiag2outvald.DOL
w 3554 -1685 100 0 n#32 eais.ins1fgdiag2valc.VAL 3456 -1696 3712 -1696 eaos.ins1fgdiag2outvalc.DOL
w 3554 -1493 100 0 n#33 eais.ins1fgdiag2valb.VAL 3456 -1504 3712 -1504 eaos.ins1fgdiag2outvalb.DOL
w 3234 523 100 0 n#34 eais.ins1fgdiag1valq.FLNK 3456 640 3520 640 3520 512 3008 512 3008 432 3200 432 eais.ins2fgdiag1valb.SLNK
w 4354 1355 100 0 n#35 eaos.ins2fgdiag1outvalb.VAL 3968 384 4192 384 4192 1344 4576 1344 egenSubE.fgDiag1P2.INPH
w 3234 331 100 0 n#36 eais.ins2fgdiag1valb.FLNK 3456 448 3520 448 3520 320 3008 320 3008 240 3200 240 eais.ins2fgdiag1valm.SLNK
w 3554 427 100 0 n#37 eais.ins2fgdiag1valb.VAL 3456 416 3712 416 eaos.ins2fgdiag1outvalb.DOL
w 4242 1547 100 0 n#38 eaos.ins1fgdiag1outvalb.VAL 3968 1536 4576 1536 egenSubE.fgDiag1P2.INPB
w 3234 1483 100 0 n#39 eais.ins1fgdiag1valb.FLNK 3456 1600 3520 1600 3520 1472 3008 1472 3008 1392 3200 1392 eais.ins1fgdiag1valm.SLNK
w 3554 1579 100 0 n#40 eais.ins1fgdiag1valb.VAL 3456 1568 3712 1568 eaos.ins1fgdiag1outvalb.DOL
w 4130 -565 100 0 n#41 eaos.ins2fgdiag1outvalq.VAL 3968 -576 4352 -576 4352 1184 4576 1184 egenSubE.fgDiag1P2.INPM
w 4114 -373 100 0 n#42 eaos.ins2fgdiag1outvalp.VAL 3968 -384 4320 -384 4320 1216 4576 1216 egenSubE.fgDiag1P2.INPL
w 4098 -181 100 0 n#43 eaos.ins2fgdiag1outvalo.VAL 3968 -192 4288 -192 4288 1248 4576 1248 egenSubE.fgDiag1P2.INPK
w 4386 1291 100 0 n#44 eaos.ins2fgdiag1outvaln.VAL 3968 0 4256 0 4256 1280 4576 1280 egenSubE.fgDiag1P2.INPJ
w 4370 1323 100 0 n#45 eaos.ins2fgdiag1outvalm.VAL 3968 192 4224 192 4224 1312 4576 1312 egenSubE.fgDiag1P2.INPI
w 3234 -437 100 0 n#46 eais.ins2fgdiag1valp.FLNK 3456 -320 3520 -320 3520 -448 3008 -448 3008 -528 3200 -528 eais.ins2fgdiag1valq.SLNK
w 3234 -245 100 0 n#47 eais.ins2fgdiag1valo.FLNK 3456 -128 3520 -128 3520 -256 3008 -256 3008 -336 3200 -336 eais.ins2fgdiag1valp.SLNK
w 3234 -53 100 0 n#48 eais.ins2fgdiag1valn.FLNK 3456 64 3520 64 3520 -64 3008 -64 3008 -144 3200 -144 eais.ins2fgdiag1valo.SLNK
w 3234 139 100 0 n#49 eais.ins2fgdiag1valm.FLNK 3456 256 3520 256 3520 128 3008 128 3008 48 3200 48 eais.ins2fgdiag1valn.SLNK
w 3554 -533 100 0 n#50 eais.ins2fgdiag1valq.VAL 3456 -544 3712 -544 eaos.ins2fgdiag1outvalq.DOL
w 3554 -341 100 0 n#51 eais.ins2fgdiag1valp.VAL 3456 -352 3712 -352 eaos.ins2fgdiag1outvalp.DOL
w 3554 -149 100 0 n#52 eais.ins2fgdiag1valo.VAL 3456 -160 3712 -160 eaos.ins2fgdiag1outvalo.DOL
w 3554 43 100 0 n#53 eais.ins2fgdiag1valn.VAL 3456 32 3712 32 eaos.ins2fgdiag1outvaln.DOL
w 3554 235 100 0 n#54 eais.ins2fgdiag1valm.VAL 3456 224 3712 224 eaos.ins2fgdiag1outvalm.DOL
w 4338 1387 100 0 n#55 eaos.ins1fgdiag1outvalq.VAL 3968 576 4160 576 4160 1376 4576 1376 egenSubE.fgDiag1P2.INPG
w 4322 1419 100 0 n#56 eaos.ins1fgdiag1outvalp.VAL 3968 768 4128 768 4128 1408 4576 1408 egenSubE.fgDiag1P2.INPF
w 4306 1451 100 0 n#57 eaos.ins1fgdiag1outvalo.VAL 3968 960 4096 960 4096 1440 4576 1440 egenSubE.fgDiag1P2.INPE
w 4290 1483 100 0 n#58 eaos.ins1fgdiag1outvaln.VAL 3968 1152 4064 1152 4064 1472 4576 1472 egenSubE.fgDiag1P2.INPD
w 4274 1515 100 0 n#59 eaos.ins1fgdiag1outvalm.VAL 3968 1344 4032 1344 4032 1504 4576 1504 egenSubE.fgDiag1P2.INPC
w 3234 715 100 0 n#60 eais.ins1fgdiag1valp.FLNK 3456 832 3520 832 3520 704 3008 704 3008 624 3200 624 eais.ins1fgdiag1valq.SLNK
w 3234 907 100 0 n#61 eais.ins1fgdiag1valo.FLNK 3456 1024 3520 1024 3520 896 3008 896 3008 816 3200 816 eais.ins1fgdiag1valp.SLNK
w 3234 1099 100 0 n#62 eais.ins1fgdiag1valn.FLNK 3456 1216 3520 1216 3520 1088 3008 1088 3008 1008 3200 1008 eais.ins1fgdiag1valo.SLNK
w 3234 1291 100 0 n#63 eais.ins1fgdiag1valm.FLNK 3456 1408 3520 1408 3520 1280 3008 1280 3008 1200 3200 1200 eais.ins1fgdiag1valn.SLNK
w 3554 619 100 0 n#64 eais.ins1fgdiag1valq.VAL 3456 608 3712 608 eaos.ins1fgdiag1outvalq.DOL
w 3554 811 100 0 n#65 eais.ins1fgdiag1valp.VAL 3456 800 3712 800 eaos.ins1fgdiag1outvalp.DOL
w 3554 1003 100 0 n#66 eais.ins1fgdiag1valo.VAL 3456 992 3712 992 eaos.ins1fgdiag1outvalo.DOL
w 3554 1195 100 0 n#67 eais.ins1fgdiag1valn.VAL 3456 1184 3712 1184 eaos.ins1fgdiag1outvaln.DOL
w 3554 1387 100 0 n#68 eais.ins1fgdiag1valm.VAL 3456 1376 3712 1376 eaos.ins1fgdiag1outvalm.DOL
w 1488 501 100 0 n#69 eais.ins2ttfvalb.FLNK 1472 496 1504 496 1504 384 1056 384 1056 288 1264 288 eais.ins2ttfvalc.SLNK
w 1250 587 100 0 n#70 eais.ins2ttfvala.FLNK 1472 688 1504 688 1504 576 1056 576 1056 480 1216 480 eais.ins2ttfvalb.SLNK
w 1250 763 100 0 n#71 eais.ins1ttfvalc.FLNK 1488 848 1504 848 1504 752 1056 752 1056 672 1216 672 eais.ins2ttfvala.SLNK
w 1250 907 100 0 n#72 eais.ins1ttfvalb.FLNK 1472 992 1504 992 1504 896 1056 896 1056 832 1232 832 eais.ins1ttfvalc.SLNK
w 1250 1067 100 0 n#73 eais.ins1ttfvala.FLNK 1440 1184 1504 1184 1504 1056 1056 1056 1056 976 1216 976 eais.ins1ttfvalb.SLNK
w 1618 283 100 0 n#74 eais.ins2ttfvalc.VAL 1520 272 1776 272 eaos.ins2ttfoutvalc.DOL
w 1570 475 100 0 n#75 eais.ins2ttfvalb.VAL 1472 464 1728 464 eaos.ins2ttfoutvalb.DOL
w 1570 667 100 0 n#76 eais.ins2ttfvala.VAL 1472 656 1728 656 eaos.ins2ttfoutvala.DOL
w 1586 827 100 0 n#77 eais.ins1ttfvalc.VAL 1488 816 1744 816 eaos.ins1ttfoutvalc.DOL
w 1570 971 100 0 n#78 eais.ins1ttfvalb.VAL 1472 960 1728 960 eaos.ins1ttfoutvalb.DOL
w 1538 1163 100 0 n#79 eais.ins1ttfvala.VAL 1440 1152 1696 1152 eaos.ins1ttfoutvala.DOL
w 2106 251 100 0 n#80 eaos.ins2ttfoutvalc.VAL 2032 240 2240 240 2240 1040 2336 1040 egenSub.ttf.INPI
w 2066 443 100 0 n#81 eaos.ins2ttfoutvalb.VAL 1984 432 2208 432 2208 1104 2336 1104 egenSub.ttf.INPH
w 2050 635 100 0 n#82 eaos.ins2ttfoutvala.VAL 1984 624 2176 624 2176 1168 2336 1168 egenSub.ttf.INPG
w 2210 1243 100 0 n#83 eaos.ins1ttfoutvalc.VAL 2000 784 2144 784 2144 1232 2336 1232 egenSub.ttf.INPF
w 2194 1307 100 0 n#84 eaos.ins1ttfoutvalb.VAL 1984 928 2112 928 2112 1296 2336 1296 egenSub.ttf.INPE
w 2162 1371 100 0 n#85 eaos.ins1ttfoutvala.VAL 1952 1120 2048 1120 2048 1360 2336 1360 egenSub.ttf.INPD
w 2178 -341 100 0 n#86 ewaves.ins1aowave.VAL 1952 -400 2080 -400 2080 -352 2336 -352 egenSub.ao.INPB
w 2194 -405 100 0 n#87 ewaves.ins2aowave.VAL 1952 -608 2112 -608 2112 -416 2336 -416 egenSub.ao.INPC
w 2178 1499 100 0 n#88 ewaves.ins1ttfwave.VAL 1952 1440 2080 1440 2080 1488 2336 1488 egenSub.ttf.INPB
w 2146 1435 100 0 n#89 ewaves.ins2ttfwave.VAL 1952 1280 2016 1280 2016 1424 2336 1424 egenSub.ttf.INPC
[cell use]
use eais 3200 -2521 100 0 ins2fgdiag2valc
xform 0 3328 -2448
p 2944 -2450 100 0 0 SCAN:.5 second
p 2736 -2416 100 0 -1 def(INP):$(wfsIns2)$(subsys)fgDiag2P2.VALC
p 3168 -2416 75 1280 -1 pproc(INP):CA
p 3168 -2480 75 1280 -1 pproc(SDIS):NPP
use eais 3200 -1369 100 0 ins1fgdiag2vala
xform 0 3328 -1296
p 2944 -1298 100 0 0 SCAN:.5 second
p 2736 -1264 100 0 -1 def(INP):$(wfsIns1)$(subsys)fgDiag2P2.VALA
p 3168 -1264 75 1280 -1 pproc(INP):CA
p 3168 -1328 75 1280 -1 pproc(SDIS):NPP
use eais 3200 -2713 100 0 ins2fgdiag2vald
xform 0 3328 -2640
p 2944 -2642 100 0 0 SCAN:Passive
p 2736 -2608 100 0 -1 def(INP):$(wfsIns2)$(subsys)fgDiag2P2.VALD
p 3168 -2608 75 1280 -1 pproc(INP):CA
p 3168 -2672 75 1280 -1 pproc(SDIS):NPP
use eais 3200 -2329 100 0 ins2fgdiag2valb
xform 0 3328 -2256
p 2944 -2258 100 0 0 SCAN:Passive
p 2736 -2224 100 0 -1 def(INP):$(wfsIns2)$(subsys)fgDiag2P2.VALB
p 3168 -2224 75 1280 -1 pproc(INP):CA
p 3168 -2288 75 1280 -1 pproc(SDIS):NPP
use eais 3200 -2137 100 0 ins2fgdiag2vala
xform 0 3328 -2064
p 2944 -2066 100 0 0 SCAN:Passive
p 2736 -2032 100 0 -1 def(INP):$(wfsIns2)$(subsys)fgDiag2P2.VALA
p 3168 -2032 75 1280 -1 pproc(INP):CA
p 3168 -2096 75 1280 -1 pproc(SDIS):NPP
use eais 3200 -1945 100 0 ins1fgdiag2vald
xform 0 3328 -1872
p 2944 -1874 100 0 0 SCAN:Passive
p 2736 -1840 100 0 -1 def(INP):$(wfsIns1)$(subsys)fgDiag2P2.VALD
p 3168 -1840 75 1280 -1 pproc(INP):CA
p 3168 -1904 75 1280 -1 pproc(SDIS):NPP
use eais 3200 -1753 100 0 ins1fgdiag2valc
xform 0 3328 -1680
p 2944 -1682 100 0 0 SCAN:Passive
p 2736 -1648 100 0 -1 def(INP):$(wfsIns1)$(subsys)fgDiag2P2.VALC
p 3168 -1648 75 1280 -1 pproc(INP):CA
p 3168 -1712 75 1280 -1 pproc(SDIS):NPP
use eais 3200 -1561 100 0 ins1fgdiag2valb
xform 0 3328 -1488
p 2944 -1490 100 0 0 SCAN:Passive
p 2736 -1456 100 0 -1 def(INP):$(wfsIns1)$(subsys)fgDiag2P2.VALB
p 3168 -1456 75 1280 -1 pproc(INP):CA
p 3168 -1520 75 1280 -1 pproc(SDIS):NPP
use eais 3200 359 100 0 ins2fgdiag1valb
xform 0 3328 432
p 2944 430 100 0 0 SCAN:.5 second
p 2736 464 100 0 -1 def(INP):$(wfsIns2)$(subsys)fgDiag1P2.VALB
p 3168 464 75 1280 -1 pproc(INP):CA
p 3168 400 75 1280 -1 pproc(SDIS):NPP
use eais 3200 1511 100 0 ins1fgdiag1valb
xform 0 3328 1584
p 2944 1582 100 0 0 SCAN:.5 second
p 2736 1616 100 0 -1 def(INP):$(wfsIns1)$(subsys)fgDiag1P2.VALB
p 3168 1616 75 1280 -1 pproc(INP):CA
p 3168 1552 75 1280 -1 pproc(SDIS):NPP
use eais 3200 -601 100 0 ins2fgdiag1valq
xform 0 3328 -528
p 2944 -530 100 0 0 SCAN:Passive
p 2736 -496 100 0 -1 def(INP):$(wfsIns2)$(subsys)fgDiag1P2.VALQ
p 3168 -496 75 1280 -1 pproc(INP):CA
p 3168 -560 75 1280 -1 pproc(SDIS):NPP
use eais 3200 -409 100 0 ins2fgdiag1valp
xform 0 3328 -336
p 2720 -304 100 0 -1 def(FLNK):$(wfsIns2)$(subsys)fgDiag1P2.VALP
p 3168 -304 75 1280 -1 pproc(INP):CA
use eais 3200 -217 100 0 ins2fgdiag1valo
xform 0 3328 -144
p 2944 -146 100 0 0 SCAN:Passive
p 2736 -112 100 0 -1 def(INP):$(wfsIns2)$(subsys)fgDiag1P2.VALO
p 3168 -112 75 1280 -1 pproc(INP):CA
p 3168 -176 75 1280 -1 pproc(SDIS):NPP
use eais 3200 -25 100 0 ins2fgdiag1valn
xform 0 3328 48
p 2944 46 100 0 0 SCAN:Passive
p 2736 80 100 0 -1 def(INP):$(wfsIns2)$(subsys)fgDiag1P2.VALN
p 3168 80 75 1280 -1 pproc(INP):CA
p 3168 16 75 1280 -1 pproc(SDIS):NPP
use eais 3200 167 100 0 ins2fgdiag1valm
xform 0 3328 240
p 2944 238 100 0 0 SCAN:Passive
p 2736 272 100 0 -1 def(INP):$(wfsIns2)$(subsys)fgDiag1P2.VALM
p 3168 272 75 1280 -1 pproc(INP):CA
p 3168 208 75 1280 -1 pproc(SDIS):NPP
use eais 3200 551 100 0 ins1fgdiag1valq
xform 0 3328 624
p 2944 622 100 0 0 SCAN:Passive
p 2736 656 100 0 -1 def(INP):$(wfsIns1)$(subsys)fgDiag1P2.VALQ
p 3168 656 75 1280 -1 pproc(INP):CA
p 3168 592 75 1280 -1 pproc(SDIS):NPP
use eais 3200 743 100 0 ins1fgdiag1valp
xform 0 3328 816
p 2944 814 100 0 0 SCAN:Passive
p 2736 848 100 0 -1 def(INP):$(wfsIns1)$(subsys)fgDiag1P2.VALP
p 3168 848 75 1280 -1 pproc(INP):CA
p 3168 784 75 1280 -1 pproc(SDIS):NPP
use eais 3200 935 100 0 ins1fgdiag1valo
xform 0 3328 1008
p 2944 1006 100 0 0 SCAN:Passive
p 2736 1040 100 0 -1 def(INP):$(wfsIns1)$(subsys)fgDiag1P2.VALO
p 3168 1040 75 1280 -1 pproc(INP):CA
p 3168 976 75 1280 -1 pproc(SDIS):NPP
use eais 3200 1127 100 0 ins1fgdiag1valn
xform 0 3328 1200
p 2944 1198 100 0 0 SCAN:Passive
p 2736 1232 100 0 -1 def(INP):$(wfsIns1)$(subsys)fgDiag1P2.VALN
p 3168 1232 75 1280 -1 pproc(INP):CA
p 3168 1168 75 1280 -1 pproc(SDIS):NPP
use eais 3200 1319 100 0 ins1fgdiag1valm
xform 0 3328 1392
p 2944 1390 100 0 0 SCAN:Passive
p 2736 1424 100 0 -1 def(INP):$(wfsIns1)$(subsys)fgDiag1P2.VALM
p 3168 1424 75 1280 -1 pproc(INP):CA
p 3168 1360 75 1280 -1 pproc(SDIS):NPP
use eais 1264 215 100 0 ins2ttfvalc
xform 0 1392 288
p 1008 286 100 0 0 SCAN:Passive
p 880 320 100 0 -1 def(INP):$(wfsIns2)$(subsys)ttf.VALC
p 1232 320 75 1280 -1 pproc(INP):CA
p 1232 256 75 1280 -1 pproc(SDIS):NPP
use eais 1216 407 100 0 ins2ttfvalb
xform 0 1344 480
p 960 478 100 0 0 SCAN:Passive
p 832 512 100 0 -1 def(INP):$(wfsIns2)$(subsys)ttf.VALB
p 1184 512 75 1280 -1 pproc(INP):CA
p 1184 448 75 1280 -1 pproc(SDIS):NPP
use eais 1216 599 100 0 ins2ttfvala
xform 0 1344 672
p 960 670 100 0 0 SCAN:Passive
p 832 704 100 0 -1 def(INP):$(wfsIns2)$(subsys)ttf.VALA
p 1184 704 75 1280 -1 pproc(INP):CA
p 1184 640 75 1280 -1 pproc(SDIS):NPP
use eais 1232 759 100 0 ins1ttfvalc
xform 0 1360 832
p 976 830 100 0 0 SCAN:Passive
p 848 864 100 0 -1 def(INP):$(wfsIns1)$(subsys)ttf.VALC
p 1200 864 75 1280 -1 pproc(INP):CA
p 1200 800 75 1280 -1 pproc(SDIS):NPP
use eais 1216 903 100 0 ins1ttfvalb
xform 0 1344 976
p 960 974 100 0 0 SCAN:Passive
p 832 1008 100 0 -1 def(INP):$(wfsIns1)$(subsys)ttf.VALB
p 1184 1008 75 1280 -1 pproc(INP):CA
p 1184 944 75 1280 -1 pproc(SDIS):NPP
use eais 1184 1095 100 0 ins1ttfvala
xform 0 1312 1168
p 928 1166 100 0 0 SCAN:.2 second
p 800 1200 100 0 -1 def(INP):$(wfsIns1)$(subsys)ttf.VALA
p 1152 1200 75 1280 -1 pproc(INP):CA
p 1152 1136 75 1280 -1 pproc(SDIS):NPP
use eais 960 -1689 100 0 ins1aoZvale
xform 0 1088 -1616
p 704 -1618 100 0 0 SCAN:Passive
p 544 -1584 100 0 -1 def(INP):$(wfsIns1)$(subsys)aoZ.VALE
p 928 -1584 75 1280 -1 pproc(INP):CA
p 928 -1648 75 1280 -1 pproc(SDIS):NPP
use eais 960 -1497 100 0 ins1aoZvald
xform 0 1088 -1424
p 704 -1426 100 0 0 SCAN:.5 second
p 560 -1392 100 0 -1 def(INP):$(wfsIns1)$(subsys)aoZ.VALD
p 928 -1392 75 1280 -1 pproc(INP):CA
p 928 -1456 75 1280 -1 pproc(SDIS):NPP
use eais 960 -2073 100 0 ins2aoZvale
xform 0 1088 -2000
p 704 -2002 100 0 0 SCAN:Passive
p 544 -1968 100 0 -1 def(INP):$(wfsIns2)$(subsys)aoZ.VALE
p 928 -1968 75 1280 -1 pproc(INP):CA
p 928 -2032 75 1280 -1 pproc(SDIS):NPP
use eais 960 -1881 100 0 ins2aoZvald
xform 0 1088 -1808
p 704 -1810 100 0 0 SCAN:.5 second
p 560 -1776 100 0 -1 def(INP):$(wfsIns2)$(subsys)aoZ.VALD
p 928 -1776 75 1280 -1 pproc(INP):CA
p 928 -1840 75 1280 -1 pproc(SDIS):NPP
use eaos 3712 -2585 100 0 insfgdiag2outvalc
xform 0 3840 -2496
p 3456 -2514 100 0 0 OMSL:closed_loop
p 3456 -2386 100 0 0 SCAN:Passive
p 3312 -2464 100 0 0 def(DOL):0.000000000000000e+00
p 3680 -2464 75 1280 -1 pproc(DOL):NPP
use eaos 3712 -1433 100 0 ins1fgdiag2outvala
xform 0 3840 -1344
p 3456 -1362 100 0 0 OMSL:closed_loop
p 3456 -1234 100 0 0 SCAN:Passive
p 3312 -1312 100 0 0 def(DOL):0.000000000000000e+00
p 3680 -1312 75 1280 -1 pproc(DOL):NPP
use eaos 3712 -2777 100 0 ins2fgdiag2outvald
xform 0 3840 -2688
p 3456 -2706 100 0 0 OMSL:closed_loop
p 3456 -2578 100 0 0 SCAN:Passive
p 3312 -2656 100 0 0 def(DOL):0.000000000000000e+00
p 3680 -2656 75 1280 -1 pproc(DOL):NPP
use eaos 3712 -2393 100 0 ins2fgdiag2outvalb
xform 0 3840 -2304
p 3456 -2322 100 0 0 OMSL:closed_loop
p 3456 -2194 100 0 0 SCAN:Passive
p 3312 -2272 100 0 0 def(DOL):0.000000000000000e+00
p 3680 -2272 75 1280 -1 pproc(DOL):NPP
use eaos 3712 -2201 100 0 ins2fgdiag2outvala
xform 0 3840 -2112
p 3456 -2130 100 0 0 OMSL:closed_loop
p 3456 -2002 100 0 0 SCAN:Passive
p 3312 -2080 100 0 0 def(DOL):0.000000000000000e+00
p 3680 -2080 75 1280 -1 pproc(DOL):NPP
use eaos 3712 -2009 100 0 ins1fgdiag2outvald
xform 0 3840 -1920
p 3456 -1938 100 0 0 OMSL:closed_loop
p 3456 -1810 100 0 0 SCAN:Passive
p 3312 -1888 100 0 0 def(DOL):0.000000000000000e+00
p 3680 -1888 75 1280 -1 pproc(DOL):NPP
use eaos 3712 -1817 100 0 ins1fgdiag2outvalc
xform 0 3840 -1728
p 3456 -1746 100 0 0 OMSL:closed_loop
p 3456 -1618 100 0 0 SCAN:Passive
p 3312 -1696 100 0 0 def(DOL):0.000000000000000e+00
p 3680 -1696 75 1280 -1 pproc(DOL):NPP
use eaos 3712 -1625 100 0 ins1fgdiag2outvalb
xform 0 3840 -1536
p 3456 -1554 100 0 0 OMSL:closed_loop
p 3456 -1426 100 0 0 SCAN:Passive
p 3312 -1504 100 0 0 def(DOL):0.000000000000000e+00
p 3680 -1504 75 1280 -1 pproc(DOL):NPP
use eaos 3712 295 100 0 ins2fgdiag1outvalb
xform 0 3840 384
p 3456 366 100 0 0 OMSL:closed_loop
p 3456 494 100 0 0 SCAN:Passive
p 3312 416 100 0 0 def(DOL):0.000000000000000e+00
p 3680 416 75 1280 -1 pproc(DOL):NPP
use eaos 3712 1447 100 0 ins1fgdiag1outvalb
xform 0 3840 1536
p 3456 1518 100 0 0 OMSL:closed_loop
p 3456 1646 100 0 0 SCAN:Passive
p 3312 1568 100 0 0 def(DOL):0.000000000000000e+00
p 3680 1568 75 1280 -1 pproc(DOL):NPP
use eaos 3712 -665 100 0 ins2fgdiag1outvalq
xform 0 3840 -576
p 3456 -594 100 0 0 OMSL:closed_loop
p 3456 -466 100 0 0 SCAN:Passive
p 3312 -544 100 0 0 def(DOL):0.000000000000000e+00
p 3680 -544 75 1280 -1 pproc(DOL):NPP
use eaos 3712 -473 100 0 ins2fgdiag1outvalp
xform 0 3840 -384
p 3456 -402 100 0 0 OMSL:closed_loop
p 3456 -274 100 0 0 SCAN:Passive
p 3312 -352 100 0 0 def(DOL):0.000000000000000e+00
p 3680 -352 75 1280 -1 pproc(DOL):NPP
use eaos 3712 -281 100 0 ins2fgdiag1outvalo
xform 0 3840 -192
p 3456 -210 100 0 0 OMSL:closed_loop
p 3456 -82 100 0 0 SCAN:Passive
p 3312 -160 100 0 0 def(DOL):0.000000000000000e+00
p 3680 -160 75 1280 -1 pproc(DOL):NPP
use eaos 3712 -89 100 0 ins2fgdiag1outvaln
xform 0 3840 0
p 3456 -18 100 0 0 OMSL:closed_loop
p 3456 110 100 0 0 SCAN:Passive
p 3312 32 100 0 0 def(DOL):0.000000000000000e+00
p 3680 32 75 1280 -1 pproc(DOL):NPP
use eaos 3712 103 100 0 ins2fgdiag1outvalm
xform 0 3840 192
p 3456 174 100 0 0 OMSL:closed_loop
p 3456 302 100 0 0 SCAN:Passive
p 3312 224 100 0 0 def(DOL):0.000000000000000e+00
p 3680 224 75 1280 -1 pproc(DOL):NPP
use eaos 3712 487 100 0 ins1fgdiag1outvalq
xform 0 3840 576
p 3456 558 100 0 0 OMSL:closed_loop
p 3456 686 100 0 0 SCAN:Passive
p 3312 608 100 0 0 def(DOL):0.000000000000000e+00
p 3680 608 75 1280 -1 pproc(DOL):NPP
use eaos 3712 679 100 0 ins1fgdiag1outvalp
xform 0 3840 768
p 3456 750 100 0 0 OMSL:closed_loop
p 3456 878 100 0 0 SCAN:Passive
p 3312 800 100 0 0 def(DOL):0.000000000000000e+00
p 3680 800 75 1280 -1 pproc(DOL):NPP
use eaos 3712 871 100 0 ins1fgdiag1outvalo
xform 0 3840 960
p 3456 942 100 0 0 OMSL:closed_loop
p 3456 1070 100 0 0 SCAN:Passive
p 3312 992 100 0 0 def(DOL):0.000000000000000e+00
p 3680 992 75 1280 -1 pproc(DOL):NPP
use eaos 3712 1063 100 0 ins1fgdiag1outvaln
xform 0 3840 1152
p 3456 1134 100 0 0 OMSL:closed_loop
p 3456 1262 100 0 0 SCAN:Passive
p 3312 1184 100 0 0 def(DOL):0.000000000000000e+00
p 3680 1184 75 1280 -1 pproc(DOL):NPP
use eaos 3712 1255 100 0 ins1fgdiag1outvalm
xform 0 3840 1344
p 3456 1326 100 0 0 OMSL:closed_loop
p 3456 1454 100 0 0 SCAN:Passive
p 3312 1376 100 0 0 def(DOL):0.000000000000000e+00
p 3680 1376 75 1280 -1 pproc(DOL):NPP
use eaos 1776 151 100 0 ins2ttfoutvalc
xform 0 1904 240
p 1520 222 100 0 0 OMSL:closed_loop
p 1520 350 100 0 0 SCAN:Passive
p 1376 272 100 0 0 def(DOL):0.000000000000000e+00
p 1744 272 75 1280 -1 pproc(DOL):NPP
use eaos 1728 343 100 0 ins2ttfoutvalb
xform 0 1856 432
p 1472 414 100 0 0 OMSL:closed_loop
p 1472 542 100 0 0 SCAN:Passive
p 1328 464 100 0 0 def(DOL):0.000000000000000e+00
p 1696 464 75 1280 -1 pproc(DOL):NPP
use eaos 1728 535 100 0 ins2ttfoutvala
xform 0 1856 624
p 1472 606 100 0 0 OMSL:closed_loop
p 1472 734 100 0 0 SCAN:Passive
p 1328 656 100 0 0 def(DOL):0.000000000000000e+00
p 1696 656 75 1280 -1 pproc(DOL):NPP
use eaos 1744 695 100 0 ins1ttfoutvalc
xform 0 1872 784
p 1488 766 100 0 0 OMSL:closed_loop
p 1488 894 100 0 0 SCAN:Passive
p 1344 816 100 0 0 def(DOL):0.000000000000000e+00
p 1712 816 75 1280 -1 pproc(DOL):NPP
use eaos 1728 839 100 0 ins1ttfoutvalb
xform 0 1856 928
p 1472 910 100 0 0 OMSL:closed_loop
p 1472 1038 100 0 0 SCAN:Passive
p 1328 960 100 0 0 def(DOL):0.000000000000000e+00
p 1696 960 75 1280 -1 pproc(DOL):NPP
use eaos 1696 1031 100 0 ins1ttfoutvala
xform 0 1824 1120
p 1440 1102 100 0 0 OMSL:closed_loop
p 1440 1230 100 0 0 SCAN:Passive
p 1296 1152 100 0 0 def(DOL):0.000000000000000e+00
p 1664 1152 75 1280 -1 pproc(DOL):NPP
use eaos 1472 -1753 100 0 ins1aoZoutvale
xform 0 1600 -1664
p 1216 -1682 100 0 0 OMSL:closed_loop
p 1216 -1554 100 0 0 SCAN:Passive
p 1072 -1632 100 0 0 def(DOL):0.000000000000000e+00
p 1440 -1632 75 1280 -1 pproc(DOL):NPP
use eaos 1472 -1561 100 0 ins1aoZoutvald
xform 0 1600 -1472
p 1216 -1490 100 0 0 OMSL:closed_loop
p 1216 -1362 100 0 0 SCAN:Passive
p 1072 -1440 100 0 0 def(DOL):0.000000000000000e+00
p 1440 -1440 75 1280 -1 pproc(DOL):NPP
use eaos 1472 -2137 100 0 ins2aoZoutvale
xform 0 1600 -2048
p 1216 -2066 100 0 0 OMSL:closed_loop
p 1216 -1938 100 0 0 SCAN:Passive
p 1072 -2016 100 0 0 def(DOL):0.000000000000000e+00
p 1440 -2016 75 1280 -1 pproc(DOL):NPP
use eaos 1472 -1945 100 0 ins2aoZourvald
xform 0 1600 -1856
p 1216 -1874 100 0 0 OMSL:closed_loop
p 1216 -1746 100 0 0 SCAN:Passive
p 1072 -1824 100 0 0 def(DOL):0.000000000000000e+00
p 1440 -1824 75 1280 -1 pproc(DOL):NPP
use egenSub 2336 791 100 0 ttf
xform 0 2480 1216
p 2113 565 100 0 0 FTA:LONG
p 2113 565 100 0 0 FTB:DOUBLE
p 2113 533 100 0 0 FTC:DOUBLE
p 2496 1440 100 0 0 FTVA:DOUBLE
p 2113 181 100 0 0 NOB:8
p 2113 149 100 0 0 NOC:8
p 2113 -75 100 0 0 NOVJ:8
p 2048 1486 100 0 0 SCAN:.1 second
p 2416 736 100 0 1 SNAM:gwTTFtoTCS
p 2448 784 100 1024 0 name:$(top)$(subsys)$(I)
p 2288 1498 75 0 -1 pproc(INPB):PP
p 2288 1434 75 0 -1 pproc(INPC):PP
p 2288 1370 75 0 -1 pproc(INPD):PP
p 2288 1306 75 0 -1 pproc(INPE):PP
p 2288 1242 75 0 -1 pproc(INPF):PP
p 2288 1178 75 0 -1 pproc(INPG):PP
p 2288 1114 75 0 -1 pproc(INPH):PP
p 2288 1050 75 0 -1 pproc(INPI):PP
p 2048 1198 100 0 1 INAM:gwInit
use egenSub 2336 -1049 100 0 ao
xform 0 2480 -624
p 2113 -1275 100 0 0 FTA:LONG
p 2113 -1275 100 0 0 FTB:DOUBLE
p 2113 -1307 100 0 0 FTC:DOUBLE
p 2113 -1275 100 0 0 FTVA:STRING
p 2113 -1659 100 0 0 NOB:40
p 2113 -1691 100 0 0 NOC:40
p 2113 -1915 100 0 0 NOVJ:40
p 2048 -354 100 0 0 SCAN:.2 second
p 2416 -1104 100 0 1 SNAM:gwAOtoTCS
p 2448 -1056 100 1024 0 name:$(top)$(subsys)$(I)
p 2288 -342 75 0 -1 pproc(INPB):PP
p 2288 -406 75 0 -1 pproc(INPC):PP
p 2048 -642 100 0 1 INAM:gwInit
use egenSub 2336 -2201 100 0 aoZ
xform 0 2480 -1776
p 2113 -2427 100 0 0 FTA:LONG
p 2048 -1506 100 0 0 SCAN:.5 second
p 2416 -2256 100 0 1 SNAM:gwAOZtoTCS
p 2448 -2208 100 1024 0 name:$(top)$(subsys)$(I)
p 2288 -1494 75 0 -1 pproc(INPB):PP
p 2288 -1558 75 0 -1 pproc(INPC):PP
p 2288 -1622 75 0 -1 pproc(INPD):PP
p 2288 -1686 75 0 -1 pproc(INPE):PP
p 2048 -1794 100 0 1 INAM:gwInit
use egenSubE 4576 -2105 100 0 fgDiag2P2
xform 0 4720 -1680
p 4353 -2331 100 0 0 FTA:LONG
p 4736 -1808 100 0 0 SCAN:.5 second
p 4624 -2160 100 0 1 SNAM:gwFgDiag2Gensub
p 4688 -2112 100 1024 0 name:$(top)$(subsys)$(I)
p 4528 -1334 75 0 -1 pproc(INPB):PP
p 4528 -1366 75 0 -1 pproc(INPC):PP
p 4528 -1398 75 0 -1 pproc(INPD):PP
p 4528 -1430 75 0 -1 pproc(INPE):PP
p 4528 -1462 75 0 -1 pproc(INPF):PP
p 4528 -1494 75 0 -1 pproc(INPG):PP
p 4528 -1526 75 0 -1 pproc(INPH):PP
p 4528 -1558 75 0 -1 pproc(INPI):PP
p 4528 -1590 75 0 -1 pproc(INPJ):PP
p 4528 -1622 75 0 -1 pproc(INPK):PP
p 4528 -1654 75 0 -1 pproc(INPL):PP
p 4528 -1686 75 0 -1 pproc(INPM):PP
p 4288 -1698 100 0 1 INAM:gwInit
use egenSubE 4576 775 100 0 fgDiag1P2
xform 0 4720 1200
p 4353 549 100 0 0 FTA:LONG
p 4736 1072 100 0 0 SCAN:.5 second
p 4624 720 100 0 1 SNAM:gwFgDiag1Gensub
p 4688 768 100 1024 0 name:$(top)$(subsys)$(I)
p 4528 1546 75 0 -1 pproc(INPB):PP
p 4528 1514 75 0 -1 pproc(INPC):PP
p 4528 1482 75 0 -1 pproc(INPD):PP
p 4528 1450 75 0 -1 pproc(INPE):PP
p 4528 1418 75 0 -1 pproc(INPF):PP
p 4528 1386 75 0 -1 pproc(INPG):PP
p 4528 1354 75 0 -1 pproc(INPH):PP
p 4528 1322 75 0 -1 pproc(INPI):PP
p 4528 1290 75 0 -1 pproc(INPJ):PP
p 4528 1258 75 0 -1 pproc(INPK):PP
p 4528 1226 75 0 -1 pproc(INPL):PP
p 4528 1194 75 0 -1 pproc(INPM):PP
p 4288 1182 100 0 1 INAM:gwInit
use ewaves 1696 1383 100 0 ins1ttfwave
xform 0 1824 1472
p 1824 1358 100 0 0 FTVL:DOUBLE
p 1824 1390 100 0 0 NELM:8
p 1600 1486 100 0 0 SCAN:Passive
p 1280 1504 100 0 -1 def(INP):$(wfsIns1)$(subsys)ttf.VALJ
p 1664 1504 75 1280 -1 pproc(INP):CA
use ewaves 1696 1223 100 0 ins2ttfwave
xform 0 1824 1312
p 1824 1198 100 0 0 FTVL:DOUBLE
p 1824 1230 100 0 0 NELM:8
p 1280 1344 100 0 -1 def(INP):$(wfsIns2)$(subsys)ttf.VALJ
p 1664 1344 75 1280 -1 pproc(INP):CA
use ewaves 1696 -457 100 0 ins1aowave
xform 0 1824 -368
p 1824 -482 100 0 0 FTVL:DOUBLE
p 1520 -160 100 0 0 NELM:40
p 2496 -480 100 0 0 SCAN:Passive
p 1280 -336 100 0 -1 def(INP):$(wfsIns1)$(subsys)ao.VALJ
p 1664 -336 75 1280 -1 pproc(INP):CA
use ewaves 1696 -665 100 0 ins2aowave
xform 0 1824 -576
p 1824 -690 100 0 0 FTVL:DOUBLE
p 1824 -658 100 0 0 NELM:40
p 1280 -544 100 0 -1 def(INP):$(wfsIns2)$(subsys)ao.VALJ
p 1664 -544 75 1280 -1 pproc(INP):CA
use inhier -240 -329 100 0 OIWFS
xform 0 -224 -288
[comments]
