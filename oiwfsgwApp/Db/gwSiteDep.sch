[schematic2]
uniq 1
[tools]
[detail]
s 2432 80 100 0 Sciences Ltd.
s 2432 112 100 0 Observatory
s 2432 144 100 0 Copyright
s 2432 2336 100 0 $Id: agSeqSiteDepGS.sch,v 1.1 2013/05/29 16:34:16 pedro Exp $
s 2704 128 200 0 A&G Sequencer
s 2432 32 100 0 Chris Mayer
s 2720 80 100 0 Top level schematic
[cell use]
use bc200tr -160 -136 -100 0 frame
xform 0 1520 1168
use gwOiwfs 1016 976 100 0 gwOiwfs#1
xform 0 1576 1104
p 1259 1114 100 0 -1 set0:gmos gmoi:
p 1739 1114 100 0 -1 set1:nirs nirsSim:
p 1504 1112 100 0 -1 set2:niri niriSim:
p 1948 1110 100 0 -1 set3:f2 f2oi:
p 1031 991 100 0 -1 set4:nifs nifsSim:
p 1491 984 100 0 -1 set5:gsaoi gsaoi:
p 1265 984 100 0 -1 set6:nici nici
p 1706 980 100 0 -1 set7:gpi gpi:
[comments]
