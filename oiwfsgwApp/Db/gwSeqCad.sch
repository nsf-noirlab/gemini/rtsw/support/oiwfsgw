[schematic2]
uniq 118
[tools]
[detail]
w 1674 539 100 0 n#117 efanouts.efanouts#115.LNK4 1200 192 1408 192 1408 528 2000 528 2000 524 gwSendSeqComm.gwSendSeqComm#114.SLNK
w 1626 1339 100 0 n#116 efanouts.efanouts#115.LNK1 1200 288 1312 288 1312 1328 2000 1328 2000 1324 gwSendSeqComm.gwSendSeqComm#113.SLNK
w 850 715 100 0 n#111 ecad8.ecad8#23.VALH 288 704 1472 704 1472 592 2000 592 2000 584 gwSendSeqComm.gwSendSeqComm#114.VALH
w 1706 1403 100 0 n#111 junction 1472 704 1472 1392 2000 1392 2000 1384 gwSendSeqComm.gwSendSeqComm#113.VALH
w 914 843 100 0 n#110 ecad8.ecad8#23.VALF 288 832 1600 832 1600 688 2000 688 gwSendSeqComm.gwSendSeqComm#114.VALF
w 1770 1499 100 0 n#110 junction 1600 832 1600 1488 2000 1488 2000 1484 gwSendSeqComm.gwSendSeqComm#113.VALF
w 952 907 100 0 n#74 ecad8.ecad8#23.VALE 288 896 1664 896 1664 734 2000 734 gwSendSeqComm.gwSendSeqComm#114.VALE
w 1808 1547 100 0 n#74 junction 1664 896 1664 1536 2000 1536 2000 1534 gwSendSeqComm.gwSendSeqComm#113.VALE
w 968 971 100 0 n#73 ecad8.ecad8#23.VALD 288 960 1696 960 1696 784 2000 784 gwSendSeqComm.gwSendSeqComm#114.VALD
w 1824 1595 100 0 n#73 junction 1696 960 1696 1584 2000 1584 gwSendSeqComm.gwSendSeqComm#113.VALD
w 984 1035 100 0 n#72 ecad8.ecad8#23.VALC 288 1024 1728 1024 1728 834 2000 834 gwSendSeqComm.gwSendSeqComm#114.VALC
w 1840 1643 100 0 n#72 junction 1728 1024 1728 1632 2000 1632 2000 1634 gwSendSeqComm.gwSendSeqComm#113.VALC
w 1016 1099 100 0 n#71 ecad8.ecad8#23.VALB 288 1088 1792 1088 1792 880 2000 880 gwSendSeqComm.gwSendSeqComm#114.VALB
w 1872 1691 100 0 n#71 junction 1792 1088 1792 1680 2000 1680 2000 1684 gwSendSeqComm.gwSendSeqComm#113.VALB
w 1056 1163 100 0 n#70 ecad8.ecad8#23.VALA 288 1152 1872 1152 1872 944 2000 944 gwSendSeqComm.gwSendSeqComm#114.VALA
w 1912 1755 100 0 n#70 junction 1872 1152 1872 1744 2000 1744 2000 1751 gwSendSeqComm.gwSendSeqComm#113.VALA
w 1744 1445 100 0 n#62 junction 1536 768 1536 1434 2000 1434 gwSendSeqComm.gwSendSeqComm#113.VALG
w 888 779 100 0 n#62 ecad8.ecad8#23.VALG 288 768 1536 768 1536 640 2000 640 gwSendSeqComm.gwSendSeqComm#114.VALG
w 88 107 100 0 n#57 efanouts.efanouts#115.SELL 1008 288 672 288 672 96 -448 96 inhier.OIWFS.P
w 720 219 100 0 n#50 ecad8.ecad8#23.STLK 288 480 528 480 528 208 960 208 efanouts.efanouts#115.SLNK
s 368 1856 200 0 Tigger a subsystem CAD
s 1808 -336 100 0 20 October 1996
s 2032 -192 100 0 Gemini Telescope Control System
[cell use]
use efanouts 960 71 100 0 efanouts#115
xform 0 1080 224
use gwSendSeqComm 2000 455 100 0 gwSendSeqComm#114
xform 0 2112 736
p 2020 428 100 0 1 seta:wfsIns $(wfsIns1)
p 2040 408 100 0 1 setb:subc $(subcad)
use gwSendSeqComm 2000 1255 100 0 gwSendSeqComm#113
xform 0 2112 1536
p 2020 1228 100 0 1 seta:wfsIns $(wfsIns1)
p 2040 1208 100 0 1 setb:subc $(subcad)
use inhier -440 56 100 0 OIWFS
xform 0 -448 96
use ecad8 -8 392 100 0 ecad8#23
xform 0 128 896
p -80 1438 100 0 -1 DESC:Triggers a subsystem CAD on START only
p 80 1142 100 0 0 FTVA:STRING
p 80 1086 100 0 0 FTVB:STRING
p 160 686 100 0 0 FTVH:STRING
p 64 736 100 0 0 PREC:4
p 48 574 100 0 -1 SNAM:$(snam)
p 352 1118 100 0 0 def(OUTA):0.0
p 352 1054 100 0 0 def(OUTB):0.0
p 352 990 100 0 0 def(OUTC):0.0
p 352 926 100 0 0 def(OUTD):0.0
p 352 862 100 0 0 def(OUTE):0.0
p 352 798 100 0 0 def(OUTF):0.0
p 352 734 100 0 0 def(OUTG):0.0
p 368 670 100 0 0 def(OUTH):0.0
p 32 334 100 0 1 name:$(top)$(subsys)$(cad)
p 288 1120 75 768 -1 pproc(OUTA):NPP
p 288 1056 75 768 -1 pproc(OUTB):NPP
p 288 672 75 768 -1 pproc(OUTH):NPP
use bc200tr -784 -472 -100 0 frame
xform 0 896 832
[comments]
