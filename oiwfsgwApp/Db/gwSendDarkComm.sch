[schematic2]
uniq 194
[tools]
[detail]
w 1092 1211 100 2 n#193 elongouts.st.DOL 1088 1216 1088 1216 hwin.hwin#189.in
w 1098 1323 100 0 n#191 elongouts.mark.FLNK 1328 1472 1440 1472 1440 1312 816 1312 816 1184 1088 1184 elongouts.st.SLNK
w 386 1723 100 0 VALC inhier.VALC.P -240 1712 1072 1712 estringouts.valc.DOL
w 1106 1579 100 0 n#155 estringouts.valc.FLNK 1328 1696 1456 1696 1456 1568 816 1568 816 1440 1072 1440 elongouts.mark.SLNK
w 1076 1467 100 2 n#142 hwin.hwin#141.in 1072 1472 1072 1472 elongouts.mark.DOL
w 672 1691 100 0 SLNK inhier.SLNK.P -256 1488 320 1488 320 1680 1072 1680 estringouts.valc.SLNK
s 1808 -336 100 0 20 October 1996
s 2032 -192 100 0 Gemini Telescope Control System
[cell use]
use elongouts 1072 1351 100 0 mark
xform 0 1200 1440
p 912 1358 100 0 0 OMSL:closed_loop
p 1376 1408 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subcad).DIR
p 1184 1344 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1328 1408 75 768 -1 pproc(OUT):PP
use elongouts 1088 1095 100 0 st
xform 0 1216 1184
p 928 1102 100 0 0 OMSL:closed_loop
p 1408 1152 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subcad).DIR
p 1200 1088 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1344 1152 75 768 -1 pproc(OUT):PP
use hwin 896 1175 100 0 hwin#189
xform 0 992 1216
p 899 1208 100 0 -1 val(in):$(CAD_START)
use hwin 880 1431 100 0 hwin#141
xform 0 976 1472
p 883 1464 100 0 -1 val(in):$(CAD_MARK)
use estringouts 1072 1607 100 0 valc
xform 0 1200 1680
p 1008 1486 100 0 0 OMSL:closed_loop
p 1408 1664 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subcad).C
p 1184 1600 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1328 1664 75 768 -1 pproc(OUT):PP
use inhier -256 1671 100 0 VALC
xform 0 -240 1712
p -288 1648 100 0 0 IO:input
p -352 1616 100 0 0 model:connector
p -352 1584 100 0 0 revision:2.2
use inhier -272 1447 100 0 SLNK
xform 0 -256 1488
p -304 1424 100 0 0 IO:input
p -368 1392 100 0 0 model:connector
p -368 1360 100 0 0 revision:2.2
use bc200tr -784 -472 -100 0 frame
xform 0 896 832
[comments]
