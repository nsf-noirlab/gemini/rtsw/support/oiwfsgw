[schematic2]
uniq 194
[tools]
[detail]
w 1099 1358 100 2 n#193 elongouts.st.DOL 1088 1328 1088 1328 hwin.hwin#189.in
w 1114 1419 100 0 n#191 elongouts.mark.FLNK 1344 1600 1456 1600 1456 1408 832 1408 832 1296 1088 1296 elongouts.st.SLNK
w 1106 1755 100 0 n#153 estringouts.valb.FLNK 1328 1824 1456 1824 1456 1744 816 1744 816 1568 1088 1568 elongouts.mark.SLNK
w 1092 1595 100 2 n#142 hwin.hwin#141.in 1088 1600 1088 1600 elongouts.mark.DOL
w 120 1051 100 0 SLNK inhier.SLNK.P -320 1040 608 1040 608 1808 1072 1808 estringouts.valb.SLNK
w 392 1851 100 0 VALB inhier.VALB.P -240 1840 1072 1840 estringouts.valb.DOL
s 1808 -336 100 0 20 October 1996
s 2032 -192 100 0 Gemini Telescope Control System
[cell use]
use elongouts 1088 1479 100 0 mark
xform 0 1216 1568
p 928 1486 100 0 0 OMSL:closed_loop
p 1392 1536 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subcad).DIR
p 1200 1472 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1344 1536 75 768 -1 pproc(OUT):PP
use elongouts 1088 1207 100 0 st
xform 0 1216 1296
p 928 1214 100 0 0 OMSL:closed_loop
p 1408 1264 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subcad).DIR
p 1200 1200 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1344 1264 75 768 -1 pproc(OUT):PP
use hwin 896 1287 100 0 hwin#189
xform 0 992 1328
p 899 1320 100 0 -1 val(in):$(CAD_START)
use hwin 896 1559 100 0 hwin#141
xform 0 992 1600
p 899 1592 100 0 -1 val(in):$(CAD_MARK)
use estringouts 1072 1735 100 0 valb
xform 0 1200 1808
p 1008 1614 100 0 0 OMSL:closed_loop
p 1408 1760 100 0 -1 def(OUT):$(wfsIns)$(subsys)$(subcad).B
p 1184 1728 100 1024 0 name:$(top)$(wfsIns)$(subc)$(I)
p 1328 1792 75 768 -1 pproc(OUT):PP
use inhier -256 1799 100 0 VALB
xform 0 -240 1840
p -288 1776 100 0 0 IO:input
p -352 1744 100 0 0 model:connector
p -352 1712 100 0 0 revision:2.2
use inhier -336 999 100 0 SLNK
xform 0 -320 1040
p -368 976 100 0 0 IO:input
p -432 944 100 0 0 model:connector
p -432 912 100 0 0 revision:2.2
use bc200tr -784 -472 -100 0 frame
xform 0 896 832
[comments]
