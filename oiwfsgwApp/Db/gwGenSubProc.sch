[schematic2]
uniq 6
[tools]
[detail]
w 1764 27 100 2 n#1 hwin.hwin#143.in 1760 32 1760 32 elongouts.procIns2.DOL
w 1780 523 100 2 n#2 hwin.hwin#140.in 1776 528 1776 528 elongouts.procIns1.DOL
w 1394 219 100 0 n#3 efanouts.efanouts#122.LNK1 1280 208 1568 208 1568 496 1776 496 elongouts.procIns1.SLNK
w 1378 123 100 0 n#4 efanouts.efanouts#122.LNK4 1280 112 1536 112 1536 0 1760 0 elongouts.procIns2.SLNK
w 554 347 100 0 n#5 egenSub.egenSub#119.FLNK 304 336 864 336 864 128 1040 128 efanouts.efanouts#122.SLNK
w 322 -277 100 0 OIWFS inhier.OIWFS.P -224 -288 928 -288 928 208 1088 208 efanouts.efanouts#122.SELL
s 2464 -704 500 512 wfsGensub.sch
s 112 2224 500 0 OIWFS - WFS genSub records
[cell use]
use hwin 1584 487 100 0 hwin#140
xform 0 1680 528
p 1587 520 100 0 -1 val(in):1
use hwin 1568 -9 100 0 hwin#143
xform 0 1664 32
p 1571 24 100 0 -1 val(in):1
use elongouts 1776 407 100 0 procIns1
xform 0 1904 496
p 1616 414 100 0 0 OMSL:closed_loop
p 2032 432 100 0 -1 def(OUT):$(wfsIns1)$(subsys)$(subcad).PROC
p 1888 400 100 1024 0 name:$(top)$(wfsIns1)$(I)
use elongouts 1760 -89 100 0 procIns2
xform 0 1888 0
p 1600 -82 100 0 0 OMSL:closed_loop
p 2016 -64 100 0 -1 def(OUT):$(wfsIns2)$(subsys)$(subcad).PROC
p 1872 -96 100 1024 0 name:$(top)$(wfsIns2)$(I)
use inhier -240 -329 100 0 OIWFS
xform 0 -224 -288
use efanouts 1040 -9 100 0 efanouts#122
xform 0 1160 144
p 896 110 100 0 0 SELM:Specified
p 1424 -48 100 1024 1 name:$(top)$(subsys)Proc$(I)
p 1312 208 75 1280 -1 pproc(LNK1):NPP
p 1312 112 75 1280 -1 pproc(LNK4):NPP
use egenSub 16 279 100 0 egenSub#119
xform 0 160 704
p -141 1019 100 0 0 DESC:Receive TCS Data
p -207 53 100 0 0 FTA:STRING
p -207 53 100 0 0 FTB:STRING
p -207 21 100 0 0 FTC:STRING
p -207 53 100 0 0 FTVA:STRING
p -207 53 100 0 0 FTVB:STRING
p -207 21 100 0 0 FTVC:STRING
p -207 -43 100 0 0 FTVE:STRING
p -207 -107 100 0 0 FTVF:STRING
p -207 -107 100 0 0 FTVG:STRING
p -207 -587 100 0 0 NOJ:1
p -207 -395 100 0 0 NOVD:1
p -207 -523 100 0 0 NOVH:1
p 64 192 100 0 1 SCAN:Passive
p 64 224 100 0 1 SNAM:$(snam)
p 240 256 100 1024 1 name:$(top)$(subsys)$(subcad)
p 304 1050 75 0 -1 pproc(OUTA):NPP
p 304 986 75 0 -1 pproc(OUTB):NPP
p 304 922 75 0 -1 pproc(OUTC):NPP
p 304 858 75 0 -1 pproc(OUTD):NPP
p -272 686 100 0 1 INAM:gwInit
[comments]
