[schematic2]
uniq 1625
[tools]
[detail]
w 5524 155 100 0 n#1624 ebis.f2oiIntegrating.FLNK 5376 496 5472 496 5472 144 5648 144 egenSub.ReadOiInt.SLNK
w 5164 603 100 0 n#1623 ebis.gmosIntegrating.FLNK 5376 688 5440 688 5440 592 4960 592 4960 480 5120 480 ebis.f2oiIntegrating.SLNK
w 5420 475 100 0 n#1622 ebis.f2oiIntegrating.VAL 5376 464 5536 464 5536 688 5648 688 egenSub.ReadOiInt.INPC
w 5524 763 100 0 n#1621 ebis.gmosIntegrating.VAL 5376 656 5472 656 5472 752 5648 752 egenSub.ReadOiInt.INPB
w 6236 123 100 0 n#1608 egenSub.ReadOiInt.FLNK 5936 112 6608 112 6608 1088 6736 1088 esirs.measuring.SLNK
w 6220 859 100 0 n#1607 egenSub.ReadOiInt.VALA 5936 848 6576 848 6576 1248 6736 1248 esirs.measuring.INP
w 5186 827 100 0 OIWFS inhier.OIWFS.P 4784 816 5648 816 egenSub.ReadOiInt.INPA
s 7424 -112 100 0 Schematic to read A&G data
s 7168 2144 100 0 $Id: tcsReadWfs.sch,v 1.5 2013/05/10 23:30:31 gemvx Exp $
s 7424 -48 100 0 Gemini Telescope Control System
[cell use]
use egenSub 5648 55 100 0 ReadOiInt
xform 0 5792 480
p 5425 -171 100 0 0 FTA:LONG
p 5425 -171 100 0 0 FTB:LONG
p 5425 -203 100 0 0 FTC:LONG
p 5760 848 100 0 1 FTVA:LONG
p 5664 -32 100 0 1 SCAN:Passive
p 5664 0 100 0 1 SNAM:gwReadOiIntegrating
p 5760 48 100 1024 0 name:$(top)$(I)
use inhier 4768 775 100 0 OIWFS
xform 0 4784 816
use esirs 6736 999 100 0 measuring
xform 0 6944 1152
p 6976 1104 100 0 0 EGU:0/1
p 6848 992 100 1024 0 name:$(top)$(I)
use ebis 5120 407 100 0 f2oiIntegrating
xform 0 5248 480
p 5136 560 100 0 -1 DESC:Is F2 OIWFS integrating?
p 4896 318 100 0 0 ONAM:Yes
p 4896 350 100 0 0 ZNAM:No
p 4848 528 100 0 -1 def(INP):$(f2)measuring
p 5120 512 75 1280 -1 palrm(INP):MS
p 5088 512 75 1280 -1 pproc(INP):CA
use ebis 5120 599 100 0 gmosIntegrating
xform 0 5248 672
p 5136 752 100 0 -1 DESC:Is GMOS OIWFS integrating?
p 4896 510 100 0 0 ONAM:Yes
p 4896 592 100 0 1 SCAN:1 second
p 4896 542 100 0 0 ZNAM:No
p 4848 720 100 0 -1 def(INP):$(gmos)measuring
p 5120 704 75 1280 -1 palrm(INP):MS
p 5088 704 75 1280 -1 pproc(INP):CA
use bc200tr 4592 -328 -100 0 frame
xform 0 6272 976
[comments]
