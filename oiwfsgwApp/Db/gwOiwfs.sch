[schematic2]
uniq 104
[tools]
[detail]
w 1832 1771 100 0 n#86 junction 1792 1472 1792 1760 1920 1760 gwGenSubProc.gwGenSubProc#103.OIWFS
w 2376 -341 100 0 n#86 junction 2336 400 2336 -352 2464 -352 gwWfsGuide.gwWfsGuide#99.OIWFS
w 1848 -341 100 0 n#86 junction 1792 400 1792 -352 1952 -352 gwReadWfsData.gwReadWfsData#98.OIWFS
w 2376 411 100 0 n#86 junction 2336 1120 2336 400 2464 400 gwGenSub.gwGenSub#97.OIWFS
w 1832 411 100 0 n#86 junction 1792 1120 1792 400 1920 400 gwGenSub.gwGenSub#95.OIWFS
w 2104 1131 100 0 n#86 eais.OIselected.VAL 1472 1472 1792 1472 1792 1120 2464 1120 gwWfsCommands.gwWfsCommands#100.OIWFS
w 2376 1771 100 0 n#86 junction 2336 1120 2336 1760 2464 1760 gwReadWfs.gwReadWfs#94.OIWFS
s 864 2112 100 0 if any OIWFS is in use.
s 864 2160 100 0 values are used to form the overall A&G status and it selects which
s 864 2208 100 0 subsystems receive commands. It controls which subsystem's status
s 864 2240 100 0 This schematic has three main functions. It controls which A&G
s 2432 2336 100 0 $Id: agSeq.sch,v 1.19 2013/05/29 16:36:37 pedro Exp $
s 560 240 100 0 Each of the xxLoaded records above causes the
s 560 208 100 0 CAR combine records to process. This is done
s 560 176 100 0 to ensure that the CAR values are recalculated whenever a system
s 560 144 100 0 is ignored/included. This is necessary as unlike the other 
s 560 112 100 0 status records, the CAR records aren't scanned periodically.
[cell use]
use gwGenSubProc 1920 1703 100 0 gwGenSubProc#103
xform 0 2064 2000
p 1940 1676 100 0 1 seta:wfsIns1 $(gmos)
p 1960 1656 100 0 1 setb:wfsIns2 $(f2)
p 1980 1636 100 0 1 setc:subsys dc:
p 2000 1616 100 0 1 setd:subcad initSigInitFgGain
p 2020 1596 100 0 1 sete:snam gwGensubCommand
use eais 1216 1415 100 0 OIselected
xform 0 1344 1488
p 2512 1712 100 0 0 SCAN:1 second
p 960 910 100 0 0 SIMS:NO_ALARM
p 928 1520 100 0 -1 def(INP):ag:oiwfsSel.VALA
p 1184 1520 75 1280 -1 pproc(INP):NPP
use gwWfsCommands 2464 1063 100 0 gwWfsCommands#100
xform 0 2608 1360
p 2484 1036 100 0 1 seta:wfsIns1 $(gmos)
p 2504 1016 100 0 1 setb:wfsIns2 $(f2)
use gwWfsGuide 2464 -409 100 0 gwWfsGuide#99
xform 0 2608 -112
p 2484 -436 100 0 1 seta:wfsIns1 $(gmos)
p 2504 -456 100 0 1 setb:wfsIns2 $(f2)
p 2524 -476 100 0 1 setc:subsys dc:
use gwReadWfsData 1952 -409 100 0 gwReadWfsData#98
xform 0 2096 -112
p 1972 -436 100 0 1 seta:wfsIns1 $(gmos)
p 1992 -456 100 0 1 setb:wfsIns2 $(f2)
p 2012 -476 100 0 1 setc:subsys dc:
use gwGenSub 2464 343 100 0 gwGenSub#97
xform 0 2608 640
p 2484 316 100 0 1 seta:wfsIns1 $(gmos)
p 2504 296 100 0 1 setb:wfsIns2 $(f2)
p 2524 276 100 0 1 setc:subsys dc:
p 2544 256 100 0 1 setd:subcad aoZero
p 2564 236 100 0 1 sete:nvals 24
p 2584 216 100 0 1 setf:snam gwAOZero
p 2604 196 100 0 1 setg:snamSend gwAOZeroSend
use gwGenSub 1920 343 100 0 gwGenSub#95
xform 0 2064 640
p 1940 316 100 0 1 seta:wfsIns1 $(gmos)
p 1960 296 100 0 1 setb:wfsIns2 $(f2)
p 1980 276 100 0 1 setc:subsys dc:
p 2000 256 100 0 1 setd:subcad ttfZero
p 2020 236 100 0 1 sete:nvals 8
p 2040 216 100 0 1 setf:snam gwTTFZero
p 2060 196 100 0 1 setg:snamSend gwTTFZeroSend
use gwReadWfs 2416 1911 100 0 gwReadWfs#94
xform 0 2608 2000
[comments]
