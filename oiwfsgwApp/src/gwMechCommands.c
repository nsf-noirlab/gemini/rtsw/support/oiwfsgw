/*
*   FILENAME
*   tcsMechCommands.c
*
*   FUNCTION NAME(S)
*
*   gwCADwfsObserve   - start OIWFS integrating
*
*/
/* *INDENT-OFF* */
/*
 *
 */
/* *INDENT-ON* */


#include  <epicsStdioRedirect.h>
#include  <registryFunction.h>
#include  <epicsExport.h>
#include  <epicsStdioRedirect.h>
#include  <epicsStdlib.h>
#include  <epicsEvent.h>
#include  <epicsPrint.h>
#include  <iocsh.h>
#include  <string.h>
#include  <math.h>

#include  <dbDefs.h>
#include  <dbEvent.h>
#include  <genSubRecord.h>
#include  <cadRecord.h>
#include  <subRecord.h>
#include  <dbCommon.h>
#include  <recSup.h>
#include  <cad.h>

#include <tcsSubsysDecode.h>
#include <tcsSubsysCadSupport.h>

#define TTF_ARRAY_SIZE      	8
#define AO_ZERO_ARRAY_SIZE      24
#define AO_ARRAY_SIZE      	40

#define GMOS_OI      		1
#define F2_OI      		4

#define OK      		0


/*+
 *   Function name:
 *   gwCADwfsObserve 
 *
 *   Purpose:
 *   Implement the pwfs1, pwfs2 and oiwfs Observe commands
 *
 *   Description:
 *   Prior to the split of the WFS into separate crates the functionality
 *   of this command was implemented by wfsStartMeasure. The routine
 *   checks the input data for validity and then stores the exposure time.
 *   The checked data is then forwarded to the relevant WFS.
 *
 *   Invocation:re
 *   gwCADwfsObserve (pcad)
 *
 *   Parameters: (">" input, "!" modified, "<" output)  
 *      (!)   pcad   (struct cadRecord*)  Pointer to cad record structure
 *
 *   EPICS input parameters:
 *      a => No. of exposures (-1 = continuous)
 *      b => Exposure time (secs)
 *      c => Output option (None | DHS | File)
 *      d => DHS label 
 *      e => DHS output (Perm | Temp | QL)
 *      f => Path for datafiles
 *      g => Output file name
 *
 *   EPICS output paramters:
 *      vala => No. of exposures
 *      valb => Exposure time
 *      valc => Index to output option (0 | 1 | 2)
 *      vald => DHS label
 *      vale => Index to DHS output (0 | 1 | 2)
 *      valf => Path for datafiles
 *      valg => Output file name
 *
 *   Function value:
 *   (<)  status  (long) Return status, 0 = OK
 * 
 *   External functions:
 *   tcsOpticsSetAoExpTime  (tcsOptics) Save aO exposure time
 *
 *-
 */

long gwCADwfsObserve (struct cadRecord *pcad)

{

  long status ;             /* Return status */
  

  status = CAD_ACCEPT ;

  switch (pcad->dir) 
  {

   case menuDirectivePRESET :

   status = CAD_ACCEPT ;

   break ;

   case menuDirectiveSTART :

    strncpy (pcad->vala, pcad->a, MAX_STRING_SIZE) ;
    strncpy (pcad->valb, pcad->b, MAX_STRING_SIZE) ;
    strncpy (pcad->valc, pcad->c, MAX_STRING_SIZE) ;
    strncpy (pcad->vald, pcad->d, MAX_STRING_SIZE) ;
    strncpy (pcad->vale, pcad->e, MAX_STRING_SIZE) ;
    strncpy (pcad->valf, pcad->f, MAX_STRING_SIZE) ;
    strncpy (pcad->valg, pcad->g, MAX_STRING_SIZE) ;
    strncpy (pcad->valh, pcad->h, MAX_STRING_SIZE) ;

   break ;

   case menuDirectiveMARK :
   break ;

   case menuDirectiveSTOP :
   break ;

   case menuDirectiveCLEAR :
   break ;

  }

  return status ;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * ttfZero
 * 
 * Purpose:
 * Receive ttfZero information from the TCS combined with probe angle
 *
 * Invocation:
 * long ttfZero (struct genSubRecord * pgsub)
 *
 * Parameters:
 * > struct genSubRecord *pgsub pointer to gensub record
 * 
 * Return value:
 * < status   int      OK or ERROR
 *
 * Globals: 
 * None
 * 
 * External variables:
 * 
 * Requirements:
 * 
 * Author:
 * 
 * History:
 *
 */

/* INDENT ON */

/* ===================================================================== */

long gwTTFZero 
   (
   struct genSubRecord * pgsub
   )
{
   int    index = 0;
   double *ptr;
   double AGSttf[TTF_ARRAY_SIZE];           


   ptr = (double *) pgsub->j;   /* read in the array */

   for (index = 0; index < TTF_ARRAY_SIZE; index++)
      {
          AGSttf[index] = *(ptr++);
      }

   strncpy (pgsub->vala, pgsub->a, MAX_STRING_SIZE) ;
   strncpy (pgsub->valb, pgsub->b, MAX_STRING_SIZE) ;
   strncpy (pgsub->valc, pgsub->c, MAX_STRING_SIZE) ;
   memcpy(pgsub->vald, AGSttf, TTF_ARRAY_SIZE * sizeof(double)) ;
   strncpy (pgsub->vale, pgsub->a, MAX_STRING_SIZE) ;
   strncpy (pgsub->valf, pgsub->b, MAX_STRING_SIZE) ;
   strncpy (pgsub->valg, pgsub->c, MAX_STRING_SIZE) ;
   memcpy(pgsub->valh, AGSttf, TTF_ARRAY_SIZE * sizeof(double)) ;


   return (OK);
}



/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * gwttfZeroSend
 * 
 * Purpose:
 * Receive ttfZero information from the TCS combined with probe angle
 *
 * Invocation:
 * long ttfZero (struct genSubRecord * pgsub)
 *
 * Parameters:
 * > struct genSubRecord *pgsub pointer to gensub record
 * 
 * Return value:
 * < status   int      OK or ERROR
 *
 * Globals: 
 * None
 * 
 * External variables:
 * 
 * Requirements:
 * 
 * Author:
 * 
 * History:
 *
 */

/* INDENT ON */

/* ===================================================================== */

long gwTTFZeroSend
   (
   struct genSubRecord * pgsub
   )
{
   int    index = 0;
   double *ptr;
   double AGSttf[TTF_ARRAY_SIZE];           


   ptr = (double *) pgsub->d;   /* read in the array */

   for (index = 0; index < TTF_ARRAY_SIZE; index++)
      {
          AGSttf[index] = *(ptr++);
      }

   strncpy (pgsub->vala, pgsub->a, MAX_STRING_SIZE) ;
   strncpy (pgsub->valb, pgsub->b, MAX_STRING_SIZE) ;
   strncpy (pgsub->valc, pgsub->c, MAX_STRING_SIZE) ;
   memcpy(pgsub->vald, AGSttf, TTF_ARRAY_SIZE * sizeof(double)) ;

   return (OK);
}



/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * gwAoZero
*: Ambiguous

 * 
 * Purpose:
 * Receive aoZero information from the TCS combined with probe angle
 *
 * Invocation:
 * long aoZero (struct genSubRecord * pgsub)
 *
 * Parameters:
 * > struct genSubRecord *pgsub pointer to gensub record
 * 
 * Return value:
 * < status   int      OK or ERROR
 *
 * Globals: 
 * None
 * 
 * External variables:
 * 
 * Requirements:
 * 
 * Author:
 * 
 * History:
 *
 */

/* INDENT ON */

/* ===================================================================== */

long gwAOZero 
   (
   struct genSubRecord * pgsub
   )
{
   int    index = 0;
   double *ptr;
   double AGSao[AO_ZERO_ARRAY_SIZE];           


   ptr = (double *) pgsub->j;   /* read in the array */

   for (index = 0; index < AO_ZERO_ARRAY_SIZE; index++)
      {
          AGSao[index] = *(ptr++);
      }

   strncpy (pgsub->vala, pgsub->a, MAX_STRING_SIZE) ;
   strncpy (pgsub->valb, pgsub->b, MAX_STRING_SIZE) ;
   strncpy (pgsub->valc, pgsub->c, MAX_STRING_SIZE) ;
   memcpy(pgsub->vald, AGSao, AO_ZERO_ARRAY_SIZE * sizeof(double)) ;
   strncpy (pgsub->vale, pgsub->a, MAX_STRING_SIZE) ;
   strncpy (pgsub->valf, pgsub->b, MAX_STRING_SIZE) ;
   strncpy (pgsub->valg, pgsub->c, MAX_STRING_SIZE) ;
   memcpy(pgsub->valh, AGSao, AO_ZERO_ARRAY_SIZE * sizeof(double)) ;

   return (OK);
}



/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * aoZeroSend
 * 
 * Purpose:
 * Receive aoZero information from the TCS combined with probe angle
 *
 * Invocation:
 * long aoZero (struct genSubRecord * pgsub)
 *
 * Parameters:
 * > struct genSubRecord *pgsub pointer to gensub record
 * 
 * Return value:
 * < status   int      OK or ERROR
 *
 * Globals: 
 * None
 * 
 * External variables:
 * 
 * Requirements:
 * 
 * Author:
 * 
 * History:
 *
 */

/* INDENT ON */

/* ===================================================================== */

long gwAOZeroSend
   (
   struct genSubRecord * pgsub
   )
{
   int    index = 0;
   double *ptr;
   double AGSao[AO_ZERO_ARRAY_SIZE];           

   ptr = (double *) pgsub->d;   /* read in the array */

   for (index = 0; index < AO_ZERO_ARRAY_SIZE; index++)
      {
          AGSao[index] = *(ptr++);
      }

   strncpy (pgsub->vala, pgsub->a, MAX_STRING_SIZE) ;
   strncpy (pgsub->valb, pgsub->b, MAX_STRING_SIZE) ;
   strncpy (pgsub->valc, pgsub->c, MAX_STRING_SIZE) ;
   memcpy(pgsub->vald, AGSao, AO_ZERO_ARRAY_SIZE * sizeof(double)) ;

   return (OK);
}



/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * gwOutGuideGensub
 * 
 * Purpose:
 * Receive output guiding from F2 and GMOS OIWFS
 *
 * Invocation:
 * long gwOutGuideGensub (struct genSubRecord * pgsub)
 *
 * Parameters:
 * > struct genSubRecord *pgsub pointer to gensub record
 * 
 * Return value:
 * < status   int      OK or ERROR
 *
 * Globals: 
 * None
 * 
 * External variables:
 * 
 * Requirements:
 * 
 * Author:
 * 
 * History:
 *
 */

/* INDENT ON */

/* ===================================================================== */

long gwOutGuideGensub 
   (
   struct genSubRecord * pgsub
   )
{
   long *oi;
   long wfs;

   oi = (long *) pgsub->a;   /* read in the current OIWFS */
   
   wfs = *(oi);

   if (wfs == GMOS_OI) {
   	strncpy (pgsub->vala, pgsub->b, MAX_STRING_SIZE) ;
   } 
   else if (wfs == F2_OI) {
   	strncpy (pgsub->vala, pgsub->c, MAX_STRING_SIZE) ;
   } 
   else
   	strncpy (pgsub->vala, " ", MAX_STRING_SIZE) ;

   return (OK);
}




/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * gwTTFtoTCS
 * 
 * Purpose:
 * Send TTF to the TCS
 *
 * Invocation:
 * long gwTTFtoTCS (struct genSubRecord * pgsub)
 *
 * Parameters:
 * > struct gwTTFtoTCS *pgsub pointer to gensub record
 * 
 * Return value:
 * < status   int      OK or ERROR
 *
 * Globals: 
 * None
 * 
 * External variables:
 * 
 * Requirements:
 * 
 * Author:
 * 
 * History:
 *
 */

/* INDENT ON */

/* ===================================================================== */

long gwTTFtoTCS 
   (
   struct genSubRecord * pgsub
   )
{
   long *oi;
   long wfs;

   oi = (long *) pgsub->a;   /* read in the current OIWFS */
   
   wfs = *(oi);
   
   if (wfs == GMOS_OI) {
   	memcpy(pgsub->valj, pgsub->b, TTF_ARRAY_SIZE * sizeof(double)) ;
        *(double *)pgsub->vala = *(double *)pgsub->d;
        *(double *)pgsub->valb = *(double *)pgsub->e;
        *(double *)pgsub->valc = *(double *)pgsub->f;
   } 
   else if (wfs == F2_OI) {
   	memcpy(pgsub->valj, pgsub->c, TTF_ARRAY_SIZE * sizeof(double)) ;
        *(double *)pgsub->vala = *(double *)pgsub->g;
        *(double *)pgsub->valb = *(double *)pgsub->h;
        *(double *)pgsub->valc = *(double *)pgsub->i;
   } 

   return (OK);
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * gwAOtoTCS
 * 
 * Purpose:
 * Send AO to the TCS
 *
 * Invocation:
 * long gwAOtoTCS (struct genSubRecord * pgsub)
 *
 * Parameters:
 * > struct gwAOtoTCS *pgsub pointer to gensub record
 * 
 * Return value:
 * < status   int      OK or ERROR
 *
 * Globals: 
 * None
 * 
 * External variables:
 * 
 * Requirements:
 * 
 * Author:
 * 
 * History:
 *
 */

/* INDENT ON */

/* ===================================================================== */

long gwAOtoTCS 
   (
   struct genSubRecord * pgsub
   )
{
   long *oi;
   long wfs;

   oi = (long *) pgsub->a;   /* read in the current OIWFS */
   
   wfs = *(oi);
   
   if (wfs == GMOS_OI) {
   	memcpy(pgsub->valj, pgsub->b, AO_ARRAY_SIZE * sizeof(double)) ;
   } 
   else if (wfs == F2_OI) {
   	memcpy(pgsub->valj, pgsub->c, AO_ARRAY_SIZE * sizeof(double)) ;
   } 

   return (OK);
}




/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * gwFgDiag1Gensub
 * 
 * Purpose:
 * Diasplay OIWFS treshold values
 *
 * Invocation:
 * long gwFgDiag1Gensub (struct genSubRecord * pgsub)
 *
 * Parameters:
 * > struct gwFgDiag1Gensub *pgsub pointer to gensub record
 * 
 * Return value:
 * < status   int      OK or ERROR
 *
 * Globals: 
 * None
 * 
 * External variables:
 * 
 * Requirements:
 * 
 * Author:
 * 
 * History:
 *
 */

/* INDENT ON */

/* ===================================================================== */

long gwFgDiag1Gensub 
   (
   struct genSubRecord * pgsub
   )
{
   long *oi;
   long wfs;

   oi = (long *) pgsub->a;   /* read in the current OIWFS */
   
   wfs = *(oi);
   
   if (wfs == GMOS_OI) {
        *(double *)pgsub->valb = *(double *)pgsub->b;
        *(double *)pgsub->valm = *(double *)pgsub->c;
        *(double *)pgsub->valn = *(double *)pgsub->d;
        *(double *)pgsub->valo = *(double *)pgsub->e;
        *(double *)pgsub->valp = *(double *)pgsub->f;
        *(double *)pgsub->valq = *(double *)pgsub->g;
   } 
   else if (wfs == F2_OI) {
        *(double *)pgsub->valb = *(double *)pgsub->h;
        *(double *)pgsub->valm = *(double *)pgsub->i;
        *(double *)pgsub->valn = *(double *)pgsub->j;
        *(double *)pgsub->valo = *(double *)pgsub->k;
        *(double *)pgsub->valp = *(double *)pgsub->l;
        *(double *)pgsub->valq = *(double *)pgsub->m;
   } 

   return (OK);
}





/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * gwFgDiag2Gensub
 * 
 * Purpose:
 * Diasplay OIWFS treshold values
 *
 * Invocation:
 * long gwFgDiag2Gensub (struct genSubRecord * pgsub)
 *
 * Parameters:
 * > struct gwFgDiag2Gensub *pgsub pointer to gensub record
 * 
 * Return value:
 * < status   int      OK or ERROR
 *
 * Globals: 
 * None
 * 
 * External variables:
 * 
 * Requirements:
 * 
 * Author:
 * 
 * History:
 *
 */

/* INDENT ON */

/* ===================================================================== */

long gwFgDiag2Gensub 
   (
   struct genSubRecord * pgsub
   )
{
   long *oi;
   long wfs;

   oi = (long *) pgsub->a;   /* read in the current OIWFS */
   
   wfs = *(oi);
   
   if (wfs == GMOS_OI) {
        *(double *)pgsub->vala = *(double *)pgsub->b;
        *(double *)pgsub->valb = *(double *)pgsub->c;
        *(double *)pgsub->valc = *(double *)pgsub->d;
        *(double *)pgsub->vald = *(double *)pgsub->e;
   } 
   else if (wfs == F2_OI) {
        *(double *)pgsub->vala = *(double *)pgsub->f;
        *(double *)pgsub->valb = *(double *)pgsub->g;
        *(double *)pgsub->valc = *(double *)pgsub->h;
        *(double *)pgsub->vald = *(double *)pgsub->i;
   } 

   return (OK);
}


/*+
 *   Function name:
 *   gwCADoiwfsSel 
 *
 *   Purpose:
 *   Select the OIWFS to which commands are sent and status received
 *
 *   Description:
 *   The oiwfSel command selects which instrument is to receive the commands
 *   sent to the OIWFS and from which instrument status is to be read. On
 *   startup the instrument is set to "NONE" so that all commands and status
 *   are handled internally by the A&G sequencer. "NONE" may also be 
 *   selected at any time if no OIWFS is in use. If "NONE" is not specified
 *   then the routine also checks which system is to be used to handle the
 *   processing of the WFS data.
 *
 *   Invocation:
 *   gwCADoiwfsSel (pcad)
 *
 *   Parameters: (">" input, "!" modified, "<" output)  
 *      (!)   pcad   (struct cadRecord*)  Pointer to cad record structure
 *
 *   EPICS input parameters:
 *      a => OIWFS to use e.g. "NONE", "GMOS", "GNIRS" etc. 
 *      b => Processing system to use "NONE", "WFS" or "AO"
 *
 *   EPICS output paramters:
 *      vala => index to OIWFS as integer
 *      valb => index of OIWFS as string 
 *      valc => index to processing system
 *      vald => original index to processing system
 *
 *   Function value:
 *   (<)  status  (long) Return status, 0 = OK
 * 
 *-
 */

long gwCADoiwfsSel (struct cadRecord *pcad)

{

  long status = 0;           /* Return status */
  static char *oiwfsNames[] = {"NONE", "GMOS",  "GNIRS",
			       "NIRI", "F2",    "NIFS",
			       "NICI", "GSAOI", "GPI",
			       NULL} ;
  static char *outNames[] = {"WFS", "AO"} ;
  static int oiwfsIndex ;
  static int outIndex ;
  static char instrument[40]; /* name of the selected instrument */
   
/* Set message prefix */
  tcsCsSetMessageN (pcad, tcsCsCadName(pcad), ": ", (char *) NULL);

  switch (pcad->dir) 
  {
    case menuDirectivePRESET :
   
/* If no instrument is specified then default to "NONE" */

      if( !tcsDcLen(pcad->a) )
      {
        oiwfsIndex = 0;
        strcpy( pcad->a, oiwfsNames[oiwfsIndex] );
        db_post_events( pcad, pcad->a, 1 );
        status = CAD_ACCEPT;
        strcpy( instrument, oiwfsNames[oiwfsIndex]); 
      }
      else 
      {
        /* If the instrument is BHROS, override it to GMOS using a temporary 
           variable. 
	   The Instrument name (BHROS in this case) will be in pcad->a
        */
	char strTemp[40]; 
        strcpy(instrument, pcad->a);
	strcpy(strTemp, pcad->a); 
	if (strcmp(pcad->a, "BHROS") == 0) {
	  strcpy(strTemp, "GMOS");
	}
	
 
        if( tcsDcString (oiwfsNames, " ", strTemp, &oiwfsIndex, pcad) )
          status = CAD_REJECT;
        else if( !oiwfsIndex )
          status = CAD_ACCEPT;
        else
        {
          if( !tcsDcLen(pcad->b) )
          {
            tcsCsAppendMessage(pcad, "Must select a processing system") ;
            status = CAD_REJECT;
          }
          else
          {
            if( tcsDcString(outNames, " ", pcad->b, &outIndex, pcad) )
              status = CAD_REJECT;
            else
            {
              outIndex++;
              status = CAD_ACCEPT;
            }
          }
        }
      }
      break ;

   case menuDirectiveSTART :

    *(long *)pcad->vala = oiwfsIndex;
    sprintf(pcad->valb, "%d", oiwfsIndex);
    sprintf(pcad->valc, "%d", outIndex);
    outIndex-- ;
    sprintf(pcad->vald, "%d", outIndex);
    /* Write the name of the validated instrument on valf */
    strncpy (pcad->valf, instrument, 40);
    /* sprintf(pcad->valf, "%s", instrument);  */

   status = CAD_ACCEPT ;
   break ;

   case menuDirectiveMARK :
   status = CAD_ACCEPT ;
   break ;

   case menuDirectiveSTOP :
   status = CAD_ACCEPT ;
   break ;

   case menuDirectiveCLEAR :
   status = CAD_ACCEPT ;
   break ;

  }

  return status ;
}

/*+
 *   Function name:
 *   gwCADSeqCommand 
 *
 *   Purpose:
 *   Implement the pwfs1, pwfs2 and oiwfs Observe commands
 *
 *   Description:
 *   Prior to the split of the WFS into separate crates the functionality
 *   of this command was implemented by wfsStartMeasure. The routine
 *   checks the input data for validity and then stores the exposure time.
 *   The checked data is then forwarded to the relevant WFS.
 *
 *   Invocation:re
 *   gwCADSeqCommand (pcad)
 *
 *   Parameters: (">" input, "!" modified, "<" output)  
 *      (!)   pcad   (struct cadRecord*)  Pointer to cad record structure
 *
 *   EPICS input parameters:
 *      a => No. of exposures (-1 = continuous)
 *      b => Exposure time (secs)
 *      c => Output option (None | DHS | File)
 *      d => DHS label 
 *      e => DHS output (Perm | Temp | QL)
 *      f => Path for datafiles
 *      g => Output file name
 *
 *   EPICS output paramters:
 *      vala => No. of exposures
 *      valb => Exposure time
 *      valc => Index to output option (0 | 1 | 2)
 *      vald => DHS label
 *      vale => Index to DHS output (0 | 1 | 2)
 *      valf => Path for datafiles
 *      valg => Output file name
 *
 *   Function value:
 *   (<)  status  (long) Return status, 0 = OK
 * 
 *   External functions:
 *
 *-
 */

long gwCADSeqCommand (struct cadRecord *pcad)

{

  long status ;             /* Return status */
  static long mode; 

  status = CAD_ACCEPT ;

  switch (pcad->dir) 
  {

   case menuDirectivePRESET :

   status = CAD_ACCEPT ;

   break ;

   case menuDirectiveSTART :

    strncpy (pcad->vala, pcad->a, MAX_STRING_SIZE) ;
    strncpy (pcad->valb, pcad->b, MAX_STRING_SIZE) ;
    strncpy (pcad->valc, pcad->c, MAX_STRING_SIZE) ;
    strncpy (pcad->vald, pcad->d, MAX_STRING_SIZE) ;
    strncpy (pcad->vale, pcad->e, MAX_STRING_SIZE) ;
    strncpy (pcad->valf, pcad->f, MAX_STRING_SIZE) ;
    strncpy (pcad->valg, pcad->g, MAX_STRING_SIZE) ;
    strncpy (pcad->valh, pcad->h, MAX_STRING_SIZE) ;
    strncpy (pcad->vali, pcad->i, MAX_STRING_SIZE) ;
    strncpy (pcad->valj, pcad->j, MAX_STRING_SIZE) ;
    strncpy (pcad->valk, pcad->k, MAX_STRING_SIZE) ;
    strncpy (pcad->vall, pcad->l, MAX_STRING_SIZE) ;
    strncpy (pcad->valm, pcad->m, MAX_STRING_SIZE) ;
    strncpy (pcad->valn, pcad->n, MAX_STRING_SIZE) ;
    strncpy (pcad->valo, pcad->o, MAX_STRING_SIZE) ;
    if (tcsDcLong("output: ", pcad->p, &mode, pcad)) break;
    *(long *) pcad->valp = (long)mode;

   break ;

   case menuDirectiveMARK :
   break ;

   case menuDirectiveSTOP :
   break ;

   case menuDirectiveCLEAR :
   break ;

  }

  return status ;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * gwReadOiIntegrating
 * 
 * Purpose:
 * Receive output guiding from F2 and GMOS OIWFS
 *
 * Invocation:
 * long gwReadOiIntegrating (struct genSubRecord * pgsub)
 *
 * Parameters:
 * > struct genSubRecord *pgsub pointer to gensub record
 * 
 * Return value:
 * < status   int      OK or ERROR
 *
 * Globals: 
 * None
 * 
 * External variables:
 * 
 * Requirements:
 * 
 * Author:
 * 
 * History:
 *
 */

/* INDENT ON */

/* ===================================================================== */

long gwReadOiIntegrating 
   (
   struct genSubRecord * pgsub
   )
{
   long *temp;
   long wfs, gmosInt, gmosF2;

   temp = (long *) pgsub->a;   /* read in the current OIWFS */
   wfs = *(temp);
   temp = (long *) pgsub->b;   /* Is GMOS integrating ? */
   gmosInt = *(temp);
   temp = (long *) pgsub->c;   /* Is F2 integrating ? */
   gmosF2 = *(temp);

   if (wfs == GMOS_OI && gmosInt) {
      *(long *) pgsub->vala = 1;
   } 
   else if (wfs == F2_OI && gmosF2) {
      *(long *) pgsub->vala = 1;
   } 
   else
      *(long *) pgsub->vala = 0;

   return (OK);
}



/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * gwGensubCommand
 * 
 * Purpose:
 * Copy Gensub inputs to the outputs
 *
 * Invocation:
 * long gwGensubCommand (struct genSubRecord * pgsub)
 *
 * Parameters:
 * > struct gwGensubCommand *pgsub pointer to gensub record
 * 
 * Return value:
 * < status   int      OK or ERROR
 *
 * Globals: 
 * None
 * 
 * External variables:
 * 
 * Requirements:
 * 
 * Author:
 * 
 * History:
 *
 */

/* INDENT ON */

/* ===================================================================== */

long gwGensubCommand 
   (
   struct genSubRecord * pgsub
   )
{

   *(double *)pgsub->vala = *(double *)pgsub->a;
   *(double *)pgsub->valb = *(double *)pgsub->b;
   *(double *)pgsub->valc = *(double *)pgsub->c;
   *(double *)pgsub->vald = *(double *)pgsub->d;
   *(double *)pgsub->vale = *(double *)pgsub->e;
   *(double *)pgsub->valf = *(double *)pgsub->f;
   *(double *)pgsub->valg = *(double *)pgsub->g;
   *(double *)pgsub->valh = *(double *)pgsub->h;
   *(double *)pgsub->vali = *(double *)pgsub->i;
   *(double *)pgsub->valj = *(double *)pgsub->j;

   return (OK);
}

/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * gwAOZtoTCS
 * 
 * Purpose:
 * Send AO to the TCS
 *
 * Invocation:
 * long gwAOZtoTCS (struct genSubRecord * pgsub)
 *
 * Parameters:
 * > struct gwAOZtoTCS *pgsub pointer to gensub record
 * 
 * Return value:
 * < status   int      OK or ERROR
 *
 * Globals: 
 * None
 * 
 * External variables:
 * 
 * Requirements:
 * 
 * Author:
 * 
 * History:
 *
 */

/* INDENT ON */

/* ===================================================================== */

long gwAOZtoTCS 
   (
   struct genSubRecord * pgsub
   )
{
   long *oi;
   long wfs;

   oi = (long *) pgsub->a;   /* read in the current OIWFS */
   
   wfs = *(oi);
   
   if (wfs == GMOS_OI) {
      *(double *)pgsub->vald = *(double *)pgsub->b;
      *(double *)pgsub->vale = *(double *)pgsub->c;
   } 
   else if (wfs == F2_OI) {
      *(double *)pgsub->vald = *(double *)pgsub->d;
      *(double *)pgsub->vale = *(double *)pgsub->e;
   } 

   return (OK);
}
/* ===================================================================== */

long gwInit 
   (
   struct genSubRecord * pgsub
   )
{
return(0);
}
epicsRegisterFunction(gwAOZtoTCS);
epicsRegisterFunction(gwGensubCommand);
epicsRegisterFunction(gwReadOiIntegrating);
epicsRegisterFunction(gwCADSeqCommand);
epicsRegisterFunction(gwCADoiwfsSel);
epicsRegisterFunction(gwFgDiag2Gensub);
epicsRegisterFunction(gwFgDiag1Gensub);
epicsRegisterFunction(gwAOtoTCS);
epicsRegisterFunction(gwTTFtoTCS);
epicsRegisterFunction(gwOutGuideGensub);
epicsRegisterFunction(gwAOZeroSend);
epicsRegisterFunction(gwAOZero);
epicsRegisterFunction(gwTTFZeroSend);
epicsRegisterFunction(gwTTFZero);
epicsRegisterFunction(gwCADwfsObserve);
epicsRegisterFunction(gwInit);
